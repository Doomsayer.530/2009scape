|         |                  |                                        |
|---------|------------------|----------------------------------------|
| 644 162 | Longsword        | Render Animation Id                    |
| 644     | Integer          | Render Animation Id                    |
| 687     | 1                | enable spec bar                        |
| 687     | 0                | disable spec bar                       |
| 686     | 1                | staff combat styles                    |
| 686     | 6                | scimitar/longsword combat styles       |
| 686     | 7                | two-handed sword combat styles         |
| 686     | 10               | warhammer combat styles                |
| 686     | 11               | whip combat styles                     |
| 686     | 16               | shortbow/longbow combat syles          |
| 686     | 17               | crossbow combat styles                 |
| 749     | (levelId)        | Integer - Quest Id Requirement         |
| 750     | (level required) | Stab attack bonus                      |
| 0       | 861              | Slash attack bonus                     |
| 1       | Integer          | Crush attack bonus                     |
| 2       | Integer          | Magic attack bonus                     |
| 3       | Integer          | Range attack bonus                     |
| 4       | Integer          | Stab defense bonus                     |
| 5       | Integer          | Slash defense bonus                    |
| 6       | Integer          | Crush defense bonus                    |
| 7       | Integer          | Magic defense bonus                    |
| 8       | Integer          | Range defense bonus                    |
| 9       | Integer          | Prayer bonus                           |
| 11      | Integer          | Attack speed                           |
| 14      | Integer          | Ranged level requirement ? ( 20<- /40) |
| 23      | Integer          | Maxed skill requirement                |
| 277     | Integer          | Summoning defense bonus                |
| 417     | Integer          | Equipment tab Option (ActionButtons2)  |
| 528     | Integer          | Equipment tab Option (ActionButtons3)  |
| 529     | String           | Equipment tab Option (ActionButtons4)  |
| 530     | String           | Equipment tab Option (ActionButtons5)  |
| 531     | String           | Melee Strength bonus                   |
| 641     | String           | Magic Strength bonus                   |
| 642     | Integer          | Ranged Strength bonus                  |
| 643     | Integer          | Render Animation Id                    |
| 644     | Integer          | Magic damage                           |
| 685     | Integer          | Attack Styles Tab                      |
| 686     | Integer          | Special bar (1)                        |
| 687     | Integer          | Skill Id Requirment                    |
| 749     | Integer          | Skill Level Requirment                 |
| 750     | Integer          | Skill Id Requirment                    |
| 751     | Integer          | Skill Level Requirment                 |
| 752     | Integer          | Skill Id Requirment                    |
| 753     | Integer          | Skill Level Requirment                 |
| 754     | Integer          | Skill Id Requirment                    |
| 755     | Integer          | Skill Level Requirment                 |
| 756     | Integer          | Skill Id Requirment                    |
| 757     | Integer          | Skill Level Requirment                 |
| 758     | Integer          | Quest Id Requirement                   |
| 861     | Integer          | Absorb melee bonus                     |
| 967     | Integer          | Absorb range bonus                     |
| 968     | Integer          | Absorb magic bonus                     |
| 969     | Integer          |                                        |
| 969     | Integer          |                                        |