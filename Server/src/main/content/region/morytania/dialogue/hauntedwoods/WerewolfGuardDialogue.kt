package content.region.morytania.dialogue.hauntedwoods

import core.api.consts.Items
import core.api.consts.NPCs
import core.api.*
import core.game.dialogue.DialogueFile
import core.game.dialogue.Dialogue
import core.game.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.entity.skill.Skills
import core.game.world.map.Location
import core.plugin.Initializable
import core.tools.END_DIALOGUE

@Initializable
class WerewolfGuardDialogue(player: Player? = null) : Dialogue(player) {

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        playerl(FacialExpression.HALF_ASKING, "What's beneath the trapdoor?").also { stage = 0 }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> npcl(FacialExpression.CHILD_NORMAL, "It's an agility course designed for lycanthropes like ourselves, my friend.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.WEREWOLF_1665)
    }

}

class WerewolfGuardDialogueFile : DialogueFile() {
    override fun handle(componentID: Int, buttonID: Int) {
        npc = NPC(NPCs.WEREWOLF_1665)
        when (stage) {
            0 -> if (!inEquipment(player!!, Items.RING_OF_CHAROS_4202) && getStatLevel(player!!, Skills.AGILITY) >= 60) {
                end()
                npc(FacialExpression.CHILD_NORMAL, "You can't go down there human. If it wasn't my duty", "to guard this trapdoor, I would be relieving you of the", "burden of your life right now.")
            } else {
                npc(FacialExpression.CHILD_NORMAL, "Good luck down there, my friend. Remember, to the", "west is the main agility course, while to the east is a", "skullball course.")
                stage = 1
            }
            1 -> {
                end()
                sendMessage(player!!, "You climb down through the trapdoor.")
                teleport(player!!, Location(3549, 9865, 0))
            }
        }
    }
}