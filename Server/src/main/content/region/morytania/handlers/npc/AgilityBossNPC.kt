package content.region.morytania.handlers.npc

import core.api.animate
import core.api.consts.NPCs
import core.game.node.entity.npc.AbstractNPC
import core.game.world.map.Location
import core.plugin.Initializable
import core.tools.RandomFunction

@Initializable
class AgilityBossNPC(id: Int = 0, location: Location? = null) : AbstractNPC(id, location) {

    override fun construct(id: Int, location: Location, vararg objects: Any): AbstractNPC {
        return AgilityBossNPC(id, location)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.AGILITY_BOSS_1661)
    }

    override fun tick() {
        if (RandomFunction.random(1, 50) == 25) {
            animate(this.asNpc(), 6549)
        }
        super.tick()
    }

}