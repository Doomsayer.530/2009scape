package content.region.morytania.handlers.npc

import core.api.consts.NPCs
import core.api.sendChat
import core.game.node.entity.npc.AbstractNPC
import core.game.world.map.Location
import core.plugin.Initializable
import core.tools.RandomFunction

@Initializable
class AgilityTrainerNPC(id: Int = 0, location: Location? = null) : AbstractNPC(id, location) {

    private val forceChat = arrayOf(
        "Remember - a slow wolf is a hungry wolf!!",
        "Get on with it - you need your whiskers perking!!!!",
        "Claws first - think later.",
        "Imagine the smell of blood in your nostrils!!!",
        "I never really wanted to be an agility trainer...",
        "It'll be worth it when you hunt!!",
        "Let's see those powerful backlegs at work!!",
        "Let the bloodlust take you!!",
        "You're the slowest wolf I've ever had the misfortune to witness!!",
        "When you're done there's a human with your name on it!!"
    )
    private val lastDialogue = arrayOf(
        "Remember - no stick, no agility bonus!",
        "Bring me the stick when you've finished the course!",
        "Don't forget to give me the stick when you're done!"
    )

    override fun construct(id: Int, location: Location, vararg objects: Any): AbstractNPC {
        return AgilityTrainerNPC(id, location)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.AGILITY_TRAINER_1663, NPCs.AGILITY_TRAINER_1664)
    }

    override fun tick() {
        if (RandomFunction.random(1, 50) == 25) {
            when {
                this.id != 1664 -> sendChat(this.asNpc(), forceChat.random())
                else -> sendChat(this.asNpc(), lastDialogue.random())
            }
        }
        super.tick()
    }

}