package content.region.morytania.quest.searchmyreq

import core.game.interaction.InteractionListener
import core.plugin.Initializable

@Initializable
class InSearchOfTheMyrequeListeners : InteractionListener {
    // https://www.youtube.com/watch?v=pNz0_8VjqPc&ab_channel=GoingCrazy201
    override fun defineListeners() {}
}