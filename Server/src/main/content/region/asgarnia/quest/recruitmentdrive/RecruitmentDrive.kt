package content.region.asgarnia.quest.recruitmentdrive

import core.api.*
import core.api.consts.Items
import core.game.node.entity.player.Player
import core.game.node.entity.player.link.quest.Quest
import core.game.node.entity.skill.Skills
import core.plugin.Initializable

@Initializable
class RecruitmentDrive : Quest("Recruitment Drive", 103, 102, 1, 496, 0, 1, 2) {
    companion object {
        const val ATTRIBUTE_SPAWN_REQUEST = "respawn:falador"
        const val ATTRIBUTE_RD_STAGE_PASSED = "rd:passedstage"
        const val ATTRIBUTE_RD_STAGE_FAILED = "rd:failedstage"
        const val ATTRIBUTE_RD_CURRENT_STAGE = "/save:rd:currentstage"
        const val ATTRIBUTE_RD_STAGE_ZERO = "/save:rd:stage:0"
        const val ATTRIBUTE_RD_FIRST_STAGE = "/save:rd:stage:1"
        const val ATTRIBUTE_RD_SECOND_STAGE = "/save:rd:stage:2"
        const val ATTRIBUTE_RD_THIRD_STAGE = "/save:rd:stage:3"
        const val ATTRIBUTE_RD_FOURTH_STAGE = "/save:rd:stage:4"
        const val ATTRIBUTE_RD_MAKEOVER_VOUCHER = "/save:rd:makeover-mage"
        val RD_STAGE_ARRAY = arrayOf(ATTRIBUTE_RD_STAGE_ZERO, ATTRIBUTE_RD_FIRST_STAGE, ATTRIBUTE_RD_SECOND_STAGE, ATTRIBUTE_RD_THIRD_STAGE, ATTRIBUTE_RD_FOURTH_STAGE)
    }

    override fun drawJournal(player: Player, stage: Int) {
        super.drawJournal(player, stage)
        var line = 11
        var stage = getStage(player)

        var started = getQuestStage(player, "Recruitment Drive") > 0

        if (!started) {
            line(player, "I can start this quest by speaking to !!Sir Amik Varze??,", line++)
            line(player, "upstairs in !!Falador Castle??.", line++)
            if (isQuestComplete(player, "Druidic Ritual")) {
                line(player, "with the Druidic Ritual Quest completed,", line++, isQuestComplete(player, "Druidic Ritual"))
            } else {
                line(player, "I have to completed the !!Druidic Ritual Quest??,", line++)
            }
            if (isQuestComplete(player, "Black Knights' Fortress")) {
                line(player, "and since I have completed the Black Knights' Fortress Quest.", line++, isQuestComplete(player, "Black Knights' Fortress"))
            } else {
                line(player, "and I have to completed the !!Black Knights' Fortress Quest??.", line++)
            }
            line++
        } else {
            line(player, "!!Sir Amik Varze?? told me that he had put my name forward as", line++, true)
            line(player, "a potential member of some mysterious organisation.", line++, true)
            line++
        }

        if (stage >= 3) {
            line(player, "I went to !!Falador Park??, and met a strange old man named !!Tiffy??.", line++, true)
            line++
            line(player, "He sent me to a secret training ground,", line++, true)
            line(player, "where my wits were thoroughly tested.", line++, true)
            line++
            line(player, "Luckily, I was too smart to fall for any of their little tricks,", line++, true)
            line(player, "and passed the test with flying colours.", line++, true)
            line++
        } else if (stage >= 1) {
            line(player, "I should head to !!Falador Park?? to meet my !!Contact?? so that I", line++, false)
            line(player, "can begin my !!testing for the job??.", line++, false)
            line++
        }

        if (stage >= 4) {
            line(player, "I am now an official member of the Temple Knights,", line++, true)
            line(player, "although I have to wait for the paperwork to go through before", line++, true)
            line(player, "I can commence working for them.", line++, true)
        } else if (stage >= 3) {
            line(player, "I should talk to !!Tiffy??.", line++, true)
            line++
        }
        if (stage >= 100) {
            line++
            line(player, "<col=FF0000>QUEST COMPLETE!</col>", line)
        }
    }

    override fun finish(player: Player) {
        var ln = 10
        super.finish(player)
        player.packetDispatch.sendString("You have passed the Recruitment Drive!", 277, 4)
        player.packetDispatch.sendItemZoomOnInterface(Items.INITIATE_SALLET_5574, 230, 277, 5)

        drawReward(player, "1 Quest Point", ln++)
        drawReward(player, "1000 Prayer, Herblore and", ln++)
        drawReward(player, "Agility XP", ln++)
        drawReward(player, "Gaze of Saradomin", ln++)
        drawReward(player, "Temple Knight's Initiate Helm", ln)

        rewardXP(player, Skills.PRAYER, 1000.0)
        rewardXP(player, Skills.HERBLORE, 1000.0)
        rewardXP(player, Skills.AGILITY, 1000.0)
        addItem(player, Items.INITIATE_SALLET_5574)
        addItem(player, Items.MAKEOVER_VOUCHER_5606)
        if(!player.isMale && getAttribute(player, ATTRIBUTE_RD_MAKEOVER_VOUCHER, false)){
            addItem(player, Items.COINS_995, 3000)
            removeAttribute(player, ATTRIBUTE_RD_MAKEOVER_VOUCHER)
        }
        removeAttributes(player, ATTRIBUTE_RD_STAGE_PASSED, ATTRIBUTE_RD_STAGE_FAILED, ATTRIBUTE_RD_CURRENT_STAGE, ATTRIBUTE_RD_STAGE_ZERO, ATTRIBUTE_RD_FIRST_STAGE, ATTRIBUTE_RD_SECOND_STAGE, ATTRIBUTE_RD_THIRD_STAGE, ATTRIBUTE_RD_FOURTH_STAGE)
    }

    override fun newInstance(`object`: Any?): Quest {
        return this
    }
}
