package content.region.asgarnia.handlers.portsarim.mogre

import core.game.world.update.flag.context.Animation

object SkippyUtils {
    val VARBIT_SKIPPY_AND_THE_MOGRES_PROGRESS = 1344
    val ANIMATION_THROW = Animation(385)
    val ANIMATION_THROW_BUCKET = Animation(2450)
}