package content.region.fremennik.dialogue.lighthouse

import content.data.GodBook
import content.region.fremennik.quest.horrorfromthedeep.dialogue.JossikRewardDialogue
import core.api.consts.Items
import core.api.consts.NPCs
import core.game.dialogue.Dialogue
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable

@Initializable
class JossikDialogue(player: Player? = null) : Dialogue(player) {

    override fun open(vararg args: Any): Boolean {
        npc = args[0] as NPC
        if (player.inventory.contains(Items.RUSTY_CASKET_3849, 1)) {
            end()
            player.dialogueInterpreter.open(JossikRewardDialogue(), npc)
        } else {
            npc("Hello again, adventurer.", "What brings you this way?")
            stage = 0
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        var uncompleted: MutableList<GodBook>? = null
        when (stage) {
            0 -> {
                options("Can I see your wares?", "Have you found any prayerbooks?")
                stage++
            }

            1 -> stage = if (buttonId == 1) {
                player("Can I see your wares?")
                10
            } else {
                player("Have you found any prayerbooks?")
                20
            }

            20 -> {
                var missing = false
                for (book in GodBook.values()) {
                    if (player.getSavedData().globalData.hasCompletedGodBook(book) && !player.hasItem(book.book)) {
                        missing = true
                        player.inventory.add(book.book, player)
                        npc(
                            "As a matter of fact, I did! This book washed up on the",
                            "beach, and I recognised it as yours!"
                        )
                    }
                }
                val damaged = player.getSavedData().globalData.godBook
                if (damaged != -1 && !player.hasItem(GodBook.values()[damaged].damagedBook)) {
                    missing = true
                    player.inventory.add(GodBook.values()[damaged].damagedBook, player)
                    npc(
                        "As a matter of fact, I did! This book washed up on the",
                        "beach, and I recognised it as yours!"
                    )
                }
                if (missing) {
                    stage = 23
                    return true
                }
                uncompleted = ArrayList(5)
                for (book in GodBook.values()) {
                    if (!player.getSavedData().globalData.hasCompletedGodBook(book)) {
                        uncompleted.add(book)
                    }
                }
                var hasUncompleted = false
                for (book in GodBook.values()) {
                    if (player.hasItem(book.damagedBook)) {
                        hasUncompleted = true
                    }
                }
                if (uncompleted.size == 0 || hasUncompleted) { // all
                    // completed.
                    npc("No, sorry adventurer, I haven't.")
                    stage = 23
                    return true
                }
                npc(
                    "Funnily enough I have! I found some books in caskets",
                    "just the other day! I'll sell one to you for 5000 coins;",
                    "what do you say?"
                )
                stage++
            }

            21 -> {
                val names = arrayOfNulls<String>(uncompleted!!.size + 1)
                var i = 0
                while (i < uncompleted!!.size) {
                    names[i] = uncompleted!![i].name
                    i++
                }
                names[names.size - 1] = "Don't buy anything."
                options(names.toString())
                stage++
            }

            22 -> {
                if (buttonId - 1 > uncompleted!!.size - 1) {
                    player("Don't buy anything.")
                    stage = 23
                }
                if (!player.inventory.contains(995, 5000)) {
                    player("Sorry, I don't seem to have enough coins.")
                    stage = 23
                }
                if (player.inventory.freeSlots() == 0) {
                    player("Sorry, I don't have enough inventory space.")
                    stage = 23
                }
                val purchase = uncompleted!![buttonId - 1]
                if (purchase != null && player.inventory.remove(Item(995, 5000))) {
                    npc("Here you go!")
                    player.getSavedData().globalData.godBook = purchase.ordinal
                    player.inventory.add(purchase.damagedBook, player)
                    stage = 23
                } else {
                    end()
                }
            }

            23 -> end()
            10 -> {
                npc("Sure thing!", "I think you'll agree, my prices are remarkable!")
                stage++
            }

            11 -> {
                npc.openShop(player)
                end()
            }
        }
        return true
    }

    fun hasGodBook(): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.JOSSIK_1334)
    }
}