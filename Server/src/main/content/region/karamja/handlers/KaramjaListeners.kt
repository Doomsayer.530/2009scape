package content.region.karamja.handlers

import core.api.*
import core.api.consts.Items
import core.api.consts.NPCs
import core.api.consts.Scenery
import core.api.consts.Sounds
import core.game.dialogue.FacialExpression
import core.game.global.action.ClimbActionHandler
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.interaction.QueueStrength
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.game.node.scenery.SceneryBuilder
import core.game.system.task.Pulse
import core.game.world.GameWorld
import core.game.world.map.Location
import core.game.world.update.flag.context.Animation
import kotlin.math.min


class KaramjaListeners : InteractionListener {

    companion object {
        private val MUSA_POINT_DUNGEON = Location(2856, 9567, 0)
        private val VOLCANO_RIM = Location(2856, 3167, 0)
        private val PINEAPPLE_PLANT = intArrayOf(Scenery.PINEAPPLE_PLANT_1408, Scenery.PINEAPPLE_PLANT_1409, Scenery.PINEAPPLE_PLANT_1410, Scenery.PINEAPPLE_PLANT_1411, Scenery.PINEAPPLE_PLANT_1412, Scenery.PINEAPPLE_PLANT_1413)
        private val CUSTOM_OFFICERS = intArrayOf(NPCs.CUSTOMS_OFFICER_380, NPCs.CUSTOMS_OFFICER_381)
        private const val MUSA_POINT_DUNGEON_ENTRANCE = Scenery.ROCKS_492
        private const val MUSA_POINT_DUNGEON_EXIT = Scenery.CLIMBING_ROPE_1764
        private const val PICK_PINEAPPLE_ANIMATION = 2282
        private const val PINEAPPLE = Items.PINEAPPLE_2114
        private const val SHAKE_TREE_ANIMATION = 2572
        private const val PALM_LEAF = Items.PALM_LEAF_2339
        private const val PALM_TREE_FULL = Scenery.LEAFY_PALM_TREE_2975
        private const val PALM_TREE_EMPTY = Scenery.LEAFY_PALM_TREE_2976
        private const val BANANA_TREE = Scenery.BANANA_TREE_2078
        private const val JIMINUA = NPCs.JIMINUA_560
        private const val NOTED_PURE_ESSENCE = Items.PURE_ESSENCE_7937
        private const val TIADECHE = NPCs.TIADECHE_1164
    }

    /*
        Check requirements for interact with jungle bush.
     */

    private fun checkRequirement(player: Player): Boolean {
        val macheteId = Item(Items.MACHETE_975)
        val jadeMachete = Item(Items.JADE_MACHETE_6315)
        val opalMachete = Item(Items.OPAL_MACHETE_6313)
        val redTopazMachete = Item(Items.RED_TOPAZ_MACHETE_6317)
        return inEquipmentOrInventory(player, macheteId.id) || inEquipmentOrInventory(player, jadeMachete.id) || inEquipmentOrInventory(player, opalMachete.id) || inEquipmentOrInventory(player, redTopazMachete.id)
    }

    override fun defineListeners() {

        /*
            Interaction with jungle bush.
         */

        on(intArrayOf(2892, 2893), IntType.SCENERY, "chop-down") { player, node ->
            val toChop = node.asScenery()
            if (checkRequirement(player)) {
                GameWorld.Pulser.submit(object : Pulse(0) {
                    var ticks = 0
                    override fun pulse(): Boolean {
                        when (ticks++) {
                            0 -> player.animator.animate(Animation.create(910)).also { player.lock() }
                            1 -> player.animator.animate(Animation.create(2382))
                            2 -> SceneryBuilder.replace(toChop,
                                core.game.node.scenery.Scenery(2895, toChop.location, toChop.rotation), 20)

                            3 -> {
                                player.walkingQueue.reset()
                                player.walkingQueue.addPath(toChop.location.x, toChop.location.y, true)
                            }

                            4 -> {
                                player.unlock()
                                return true
                            }
                        }
                        return false
                    }
                })
            } else {
                sendMessage(player, "You need a machete to get through this dense jungle.")
            }
            return@on true
        }

        /*
            Interaction with Custom officers.
         */

        on(CUSTOM_OFFICERS, IntType.NPC, "pay-fare") { player, node ->
            if (!player.getQuestRepository().isComplete("Pirate's Treasure")) {
                openDialogue(player,node.asNpc().id, node)
                sendMessage(player, "You may only use the Pay-fare option after completing Pirate's Treasure.")
                return@on true
            }
            openDialogue(player, node.asNpc().id, node, true)
            return@on true
        }

        on(MUSA_POINT_DUNGEON_ENTRANCE, IntType.SCENERY, "climb-down") { player, _ ->
            ClimbActionHandler.climb(player, ClimbActionHandler.CLIMB_DOWN, MUSA_POINT_DUNGEON)
            sendMessage(player, "You climb down through the pot hole.")
            return@on true
        }

        on(MUSA_POINT_DUNGEON_EXIT, IntType.SCENERY, "climb-up") { player, _ ->
            ClimbActionHandler.climb(player, ClimbActionHandler.CLIMB_UP, VOLCANO_RIM)
            sendMessage(player, "You climb up the hanging rope...")
            sendMessage(player, "You appear on the volcano rim.")
            return@on true
        }

        on(PINEAPPLE_PLANT, IntType.SCENERY, "pick") { player, node ->
            if (!hasSpaceFor(player, Item(PINEAPPLE))) {
                sendMessage(player, "You don't have enough space in your inventory.")
                return@on true
            }
            if (node.id == Scenery.PINEAPPLE_PLANT_1413) {
                sendMessage(player, "There are no pineapples left on this plant.")
                return@on true
            }
            val last: Boolean = node.id == Scenery.PINEAPPLE_PLANT_1412
            if (addItem(player, PINEAPPLE)) {
                animate(player, PICK_PINEAPPLE_ANIMATION)
                playAudio(player, Sounds.PICK_2581, 30)
                replaceScenery(node.asScenery(), node.id + 1, if (last) 270 else 40)
                sendMessage(player, "You pick a pineapple.")
            }
            return@on true
        }

        on(BANANA_TREE, IntType.SCENERY, "search") { player, _ ->
            sendMessage(player, "There are no bananas left on the tree.")
            return@on true
        }

        on(PALM_TREE_FULL, IntType.SCENERY, "Shake") { player, node ->
            queueScript(player, 0, QueueStrength.WEAK) { stage: Int ->
                when (stage) {
                    0 -> {
                        lock(player, 2)
                        face(player, node)
                        animate(player, SHAKE_TREE_ANIMATION)
                        sendMessage(player, "You give the tree a good shake.")
                        replaceScenery(node.asScenery(), PALM_TREE_EMPTY, 60)
                        return@queueScript delayScript(player, 2)
                    }
                    1 -> {
                        produceGroundItem(player, PALM_LEAF, 1, getPathableRandomLocalCoordinate(player, 1, node.location))
                        sendMessage(player, "A palm leaf falls to the ground.")
                        return@queueScript stopExecuting(player)
                    }
                    else -> return@queueScript stopExecuting(player)
                }
            }
            return@on true
        }

        /*
            Un-note essences interaction with Jiminua NPC.
        */

        onUseWith(IntType.NPC, NOTED_PURE_ESSENCE, JIMINUA) { player, used, _ ->
            assert(used.id == Items.PURE_ESSENCE_7937)
            val ess: Int = player.inventory.getAmount(Items.PURE_ESSENCE_7937)
            val coins: Int = player.inventory.getAmount(Items.COINS_995)
            val freeSpace = player.inventory.freeSlots()
            if (ess == 0) {
                sendNPCDialogue(player, JIMINUA, "You don't have any essence for me to un-note.", FacialExpression.HALF_GUILTY)
                return@onUseWith false
            } else if (freeSpace == 0) {
                sendNPCDialogue(player, JIMINUA, "You don't have any free space.", FacialExpression.HALF_GUILTY)
                return@onUseWith false
            } else if (coins <= 1) {
                sendNPCDialogue(player, JIMINUA, "I charge 2 gold coins to un-note each pure essence.", FacialExpression.HALF_GUILTY)
                return@onUseWith false
            } else {
                val unnote = min(min(freeSpace.toDouble(), ess.toDouble()), (coins / 2).toDouble()).toInt()
                removeItem(player, Item(NOTED_PURE_ESSENCE, unnote))
                removeItem(player, (Item(Items.COINS_995, 2 * unnote)))
                addItem(player, Items.PURE_ESSENCE_7936, unnote)
                sendMessage(player,"You hand Jiminua some notes and coins, and she hands you back pure essence.")
            }
            return@onUseWith true
        }

        /*
            Trade interaction with NPC Tiadeche.
         */

        on(TIADECHE, IntType.NPC, "trade") { player, _ ->
            if (!hasRequirement(player, "Tai Bwo Wannai Trio")) return@on true
            openNpcShop(player, TIADECHE)
            return@on true
        }
    }
}