package content.region.misthalin.dialogue.draynorvillage

import core.api.addItemOrDrop
import core.api.consts.Items
import core.api.consts.NPCs
import core.api.freeSlots
import core.api.getAttribute
import core.api.removeAttribute
import core.game.dialogue.Dialogue
import core.game.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable

@Initializable
class WiseOldManDialogue(player: Player? = null) : Dialogue(player) {

    override fun open(vararg args: Any): Boolean {
        npc = args[0] as NPC
        npc("Greetings, " + player.username + ",")
        stage = 0
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> {
                if (getAttribute(player, "reclaim-qp-cape", false) || getAttribute(player, "reclaim-qp-hood", false)) {
                    return if (getAttribute(player, "reclaim-qp-cape", false) && getAttribute(
                            player,
                            "reclaim-qp-hood",
                            false
                        )
                    ) {
                        npcl(
                            FacialExpression.NEUTRAL,
                            "I assume you're looking for your items? I've placed them " + if (freeSlots(player) < 2) "at your feet." else "in your inventory."
                        )
                        removeAttribute(player, "reclaim-qp-cape")
                        removeAttribute(player, "reclaim-qp-hood")
                        addItemOrDrop(player, Items.QUEST_POINT_CAPE_9813, 1)
                        addItemOrDrop(player, Items.QUEST_POINT_HOOD_9814, 1)
                        stage = 505
                        true
                    } else if (getAttribute(player, "reclaim-qp-cape", false)) {
                        npcl(
                            FacialExpression.NEUTRAL,
                            "I assume you're looking for your Quest Point Cape? I've placed it " + if (freeSlots(player) < 1) "at your feet." else "in your inventory."
                        )
                        removeAttribute(player, "reclaim-qp-cape")
                        addItemOrDrop(player, Items.QUEST_POINT_CAPE_9813, 1)
                        stage = 505
                        true
                    } else {
                        npcl(
                            FacialExpression.NEUTRAL,
                            "I assume you're looking for your Quest Point Hood? I've placed it " + if (freeSlots(player) < 1) "at your feet." else "in your inventory."
                        )
                        removeAttribute(player, "reclaim-qp-hood")
                        addItemOrDrop(player, Items.QUEST_POINT_HOOD_9814, 1)
                        stage = 505
                        true
                    }
                }
                if (player.getQuestRepository().hasCompletedAll()) {
                    options("Quest Point Cape.", "Something else.")
                    stage = 500
                    return true
                }
                options(
                    "Is there anything I can do for you?",
                    "Could you check my things for junk, please?",
                    "I've got something I'd like you to look at."
                )
                stage = 1
            }

            500 -> when (buttonId) {
                1 -> {
                    player("I believe you are the person to talk to if I want to buy", "a Quest Point Cape?")
                    stage = 501
                }

                2 -> {
                    options(
                        "Is there anything I can do for you?",
                        "Could you check my things for junk, please?",
                        "I've got something I'd like you to look at."
                    )
                    stage = 1
                }
            }

            501 -> {
                npc(
                    "Indeed you believe rightly, " + player.username + ", and if you know that",
                    "then you'll also know that they cost 99000 coins."
                )
                stage = 502
            }

            502 -> {
                options("No, I hadn't heard that!", "Yes, so I was lead to believe.")
                stage = 503
            }

            503 -> when (buttonId) {
                1 -> {
                    player("No, I hadn't heard that!")
                    stage = 504
                }

                2 -> {
                    player("Yes, so I was lead to believe.")
                    stage = 506
                }
            }

            504 -> {
                npc("Well that's the cost, and it's not changing.")
                stage = 505
            }

            505 -> end()
            506 -> {
                if (player.inventory.freeSlots() < 2) {
                    player("I don't seem to have enough inventory space.")
                    stage = 507
                    return true
                }
                if (!player.inventory.containsItem(COINS)) {
                    player("I don't seem to have enough coins with", "me at this time.")
                    stage = 507
                    return true
                }
                stage =
                    if (player.inventory.remove(COINS) && player.inventory.add(*ITEMS)) {
                        npc("Have fun with it.")
                        507
                    } else {
                        player("I don't seem to have enough coins with", "me at this time.")
                        507
                    }
            }

            507 -> end()
            1 -> when (buttonId) {
                1 -> {
                    player("Is there anything I can do for you?")
                    stage = 10
                }

                2 -> {
                    options(
                        "Could you check my bank for junk, please?",
                        "Could you check my inventory for junk, please?"
                    )
                    stage = 20
                }

                3 -> {
                    player("I've got something I'd like you to look at.")
                    stage = 30
                }
            }

            10 -> {
                npc("Thanks, but I don't have anything I need.")
                stage = 11
            }

            11 -> end()
            20 -> {
                npc("Certainly, but I should warn you that I don't know about", "all items.")
                when (buttonId) {
                    1 -> stage = 100
                    2 -> stage = 102
                }
            }

            100 -> stage =
                if (player.bank.containsAtLeastOneItem(*UNIQUE_BOOKS) && player.bank.remove(*UNIQUE_BOOKS)) {
                    npc(FacialExpression.DISGUSTED, "That's my book! What's it doing in your bank?")
                    101
                } else {
                    npc("You seem to have no junk in your bank, sorry.")
                    101
                }

            101 -> end()
            102 -> stage =
                if (player.inventory.containsAtLeastOneItem(*UNIQUE_BOOKS) && player.inventory.remove(
                        *UNIQUE_BOOKS
                    )
                ) {
                    npc(FacialExpression.DISGUSTED, "That's my book! What's it doing in your inventory?")
                    101
                } else {
                    npc("You seem to have no junk in your inventory, sorry.")
                    101
                }

            30 -> {
                npc("Jolly good. Give it to me, and I'll tell you anything I know", "about it.")
                stage = 31
            }

            31 -> end()
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.WISE_OLD_MAN_2253, NPCs.TORTOISE_3820)
    }

    companion object {
        private val ITEMS = arrayOf(Item(9813), Item(9814))
        private val COINS = Item(995, 99000)
        private val UNIQUE_BOOKS = arrayOf(Item(5507), Item(5508), Item(7464))
    }
}