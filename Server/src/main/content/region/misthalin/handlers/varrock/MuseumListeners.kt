package content.region.misthalin.handlers.varrock

import content.region.misthalin.dialogue.varrock.museum.*
import core.api.*
import core.api.consts.Items
import core.game.dialogue.DialogueFile
import core.game.global.action.ClimbActionHandler
import core.game.global.action.DoorActionHandler
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.world.map.Location
import core.game.world.update.flag.context.Animation

class MuseumListeners : InteractionListener{

    companion object {
        private val BUTTONS = (24605..24618).toIntArray()
        private val MUSEUM_DOOR = intArrayOf(24565, 24567)
        private val MUSEUM_STAIRS = intArrayOf(24427, 24428, 24359, 24357)
        private const val MUSEUM_GATE = 24536
        private const val TOOL_RACK = 24535
    }

    override fun defineListeners() {
        on(BUTTONS, IntType.SCENERY, "study"){ player, node ->
            when(node.id){
                // East
                24605 -> openDialogue(player, TalkAboutLizards())
                24606 -> openDialogue(player, TalkAboutTortoises())
                24607 -> openDialogue(player, TalkAboutDragons())
                24608 -> openDialogue(player, TalkAboutWyverns())
                // North
                24609 -> openDialogue(player, TalkAboutCamels())
                24610 -> openDialogue(player, TalkAboutLeeches())
                24611 -> openDialogue(player, TalkAboutMoles())
                24612 -> openDialogue(player, TalkAboutPenguins())
                // West
                24613 -> openDialogue(player, TalkAboutSnails())
                24614 -> openDialogue(player, TalkAboutSnakes())
                24615 -> openDialogue(player, TalkAboutMonkeys())
                24616 -> openDialogue(player, TalkAboutSeaSlugs())
                // South
                24617 -> openDialogue(player, TalkAboutTerrorBirds())
                24618 -> openDialogue(player, TalkAboutKalphiteQueen())
            }
            return@on true
        }

        /*
            Museum stairs.
         */

        on(MUSEUM_STAIRS, IntType.SCENERY, "walk-up", "walk-down") { player, node ->
            if(node.id == 24427){
                ClimbActionHandler.climb(player, Animation(-1), Location(3258, 3452, 0))
            } else {
                ClimbActionHandler.climb(player, Animation(-1), Location(1759, 4958, 0))
            }
            if(node.id == 24359 && getUsedOption(player) == "climb-down"){
                ClimbActionHandler.climb(player, ClimbActionHandler.CLIMB_DOWN, Location(3231, 3386, 0))
            }
            if(node.id == 24357 && getUsedOption(player) == "climb-up"){
                ClimbActionHandler.climb(player, Animation(828), Location(3155, 3435, 1))
            } else {
                ClimbActionHandler.climb(player, Animation(828), Location(3188, 3354, 1))
            }
            return@on true
        }

        /*
            Museum archaeologists doors.
         */

        on(MUSEUM_DOOR, IntType.SCENERY, "open") { player, node ->
            DoorActionHandler.handleAutowalkDoor(player, node.asScenery())
            return@on true
        }

        /*
            Varrock digsite gates.
         */

        on(MUSEUM_GATE, IntType.SCENERY, "open") { player, node ->
            if (player.location.y >= 3447) {
                openDialogue(player, 5941)
            } else {
                DoorActionHandler.handleAutowalkDoor(player, node.asScenery())
            }
            return@on true
        }

        /*
            All items available for staff clean off incoming samples.
         */

        on(TOOL_RACK, IntType.SCENERY, "take") { player, _ ->
            openDialogue(player, object : DialogueFile() {
                override fun handle(componentID: Int, buttonID: Int) {
                    when (stage) {
                        0 -> {
                            setTitle(player, 5)
                            sendDialogueOptions(player, "Which tool would you like?", "Trowel", "Rock pick", "Specimen brush", "Leather gloves", "Leather boots").also { stage++ }
                        }
                        1 -> {
                            end()
                            if (freeSlots(player) < 1) {
                                sendDialogue(player, "You do not have enough inventory space to do that.")
                                return
                            }
                            when (buttonID) {
                                1 -> {
                                    sendItemDialogue(player, 676, "You take a trowel from the rack.")
                                    addItem(player, Items.TROWEL_676, 1)
                                }

                                2 -> {
                                    sendItemDialogue(player, 675, "You take a rock pick from the rack.")
                                    addItem(player, Items.ROCK_PICK_675, 1)
                                }

                                3 -> {
                                    sendItemDialogue(player, 670, "You take a specimen brush from the rack.")
                                    addItem(player, Items.SPECIMEN_BRUSH_670, 1)
                                }

                                4 -> {
                                    sendItemDialogue(player, 1059, "You take a pair of leather gloves from the rack.")
                                    addItem(player, Items.LEATHER_GLOVES_1059, 1)
                                }

                                5 -> {
                                    sendItemDialogue(player, 1061, "You take a pair of leather boots from the rack.")
                                    addItem(player, Items.LEATHER_BOOTS_1061, 1)
                                }

                                else -> sendMessage(player, "You search but find nothing of interest.")
                            }
                        }
                    }
                }
            })
            return@on true
        }
    }


}