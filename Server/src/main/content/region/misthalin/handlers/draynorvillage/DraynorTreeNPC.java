package content.region.misthalin.handlers.draynorvillage;

import core.game.node.entity.combat.ImpactHandler.HitsplatType;
import core.game.node.entity.impl.Animator.Priority;
import core.game.node.entity.npc.AbstractNPC;
import core.game.node.entity.player.Player;
import core.game.world.GameWorld;
import core.game.world.map.Location;
import core.game.world.map.RegionManager;
import core.game.world.update.flag.context.Animation;
import core.plugin.Initializable;
import core.tools.RandomFunction;

import java.util.List;

/**
 * The Draynor tree npc.
 */
@Initializable
public final class DraynorTreeNPC extends AbstractNPC {

    private static final int[] ID = {5208, 152, 5207};

    private static final Animation ANIMATION = new Animation(73, Priority.HIGH);

    private int attackDelay;

    /**
     * Instantiates a new Draynor tree npc.
     */
    public DraynorTreeNPC() {
        super(0, null, false);
    }

    private DraynorTreeNPC(int id, Location location) {
        super(id, location, false);
    }

    @Override
    public AbstractNPC construct(int id, Location location, Object... objects) {
        return new DraynorTreeNPC(id, location);
    }

    @Override
    public void tick() {
        final List<Player> players = RegionManager.getLocalPlayers(this, 1);
        if (players.size() != 0) {
            if (attackDelay < GameWorld.getTicks()) {
                for (Player p : players) {
                    faceTemporary(p, 2);
                    getAnimator().forceAnimation(ANIMATION);
                    int hit = RandomFunction.random(2);
                    p.getImpactHandler().manualHit(this, hit, hit > 0 ? HitsplatType.NORMAL : HitsplatType.MISS);
                    attackDelay = GameWorld.getTicks() + 3;
                    p.animate(p.getProperties().getDefenceAnimation());
                    return;
                }
            }
        }
        super.tick();
    }

    @Override
    public int[] getIds() {
        return ID;
    }

}
