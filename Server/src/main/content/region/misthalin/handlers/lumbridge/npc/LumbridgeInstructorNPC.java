package content.region.misthalin.handlers.lumbridge.npc;

import core.game.node.entity.npc.AbstractNPC;
import core.game.world.map.Location;
import core.plugin.Initializable;
import core.tools.RandomFunction;

/**
 * The Lumbridge instructor npc.
 */
@Initializable
public final class LumbridgeInstructorNPC extends AbstractNPC {

    private static final int[] ID = {4707, 4906, 705, 1861, 4903, 4899, 4900, 4904};

    /**
     * Instantiates a new Lumbridge instructor npc.
     */
    public LumbridgeInstructorNPC() {
        super(0, null);
    }

    private LumbridgeInstructorNPC(int id, Location location) {
        super(id, location);
    }

    @Override
    public AbstractNPC construct(int id, Location location, Object... objects) {
        return new LumbridgeInstructorNPC(id, location);
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public int getWalkRadius() {
        return 4;
    }

    @Override
    public Location getMovementDestination() {
        return getProperties().getSpawnLocation().transform(-2 + RandomFunction.random(getWalkRadius()), -2 + RandomFunction.random(getWalkRadius()), 0);
    }

    @Override
    public int[] getIds() {
        return ID;
    }

}
