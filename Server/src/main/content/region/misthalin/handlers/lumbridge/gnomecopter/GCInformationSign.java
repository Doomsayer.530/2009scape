package content.region.misthalin.handlers.lumbridge.gnomecopter;

import core.game.component.Component;
import core.game.node.entity.player.Player;

/**
 * The enum Gc information sign.
 */
public enum GCInformationSign {


    /**
     * The Entrance.
     */
    ENTRANCE("~ Gnomecopter Tours ~", "Welcome to Gnomecopter", "Tours: the unique flying", "experience!", "", "If you're a new flier, talk to", "<col=FF0000>Hieronymus</col> and prepare to be", "amazed by this triumph of", "gnomic engineering.", "", "", "Warning: all riders must be at", "least 2ft, 6ins tall.");

    private final String button;

    private final String[] info;

    GCInformationSign(String button, String... info) {
        this.button = button;
        this.info = info;
    }

    /**
     * Read.
     *
     * @param player the player
     */
    public void read(Player player) {
        player.getInterfaceManager().openSingleTab(new Component(723));
        player.getPacketDispatch().sendString(button, 723, 9);
        String information = info[0];
        for (int i = 1; i < info.length; i++) {
            information += "<br>" + info[i];
        }
        player.getPacketDispatch().sendString(information, 723, 10);
    }
}