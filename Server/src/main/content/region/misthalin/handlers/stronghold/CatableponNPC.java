package content.region.misthalin.handlers.stronghold;

import core.game.node.entity.combat.CombatStyle;
import core.game.node.entity.combat.CombatSwingHandler;
import core.game.node.entity.combat.MultiSwingHandler;
import core.game.node.entity.combat.equipment.SwitchAttack;
import core.game.node.entity.combat.spell.CombatSpell;
import core.game.node.entity.npc.AbstractNPC;
import core.game.node.entity.player.link.SpellBookManager.SpellBook;
import core.game.world.map.Location;
import core.game.world.update.flag.context.Animation;
import core.plugin.Initializable;

/**
 * The Catablepon npc.
 */
@Initializable
public final class CatableponNPC extends AbstractNPC {

    private static final int[] ID = {4397, 4398, 4399};

    private static final Animation ANIMATION = new Animation(4272);

    private final MultiSwingHandler combatHandler = new MultiSwingHandler(new SwitchAttack(CombatStyle.MELEE.getSwingHandler(), null).setUseHandler(true), new SwitchAttack(CombatStyle.MELEE.getSwingHandler(), null).setUseHandler(true), new SwitchAttack(CombatStyle.MELEE.getSwingHandler(), null).setUseHandler(true), new SwitchAttack(CombatStyle.MAGIC.getSwingHandler(), ANIMATION).setUseHandler(true));

    public CatableponNPC() {
        super(0, null);
    }

    private CatableponNPC(int id, Location location) {
        super(id, location);
        this.setAggressive(true);
    }

    @Override
    public void configure() {
        super.configure();
        super.getProperties().setSpell((CombatSpell) SpellBook.MODERN.getSpell(7));
        super.getProperties().setAutocastSpell((CombatSpell) SpellBook.MODERN.getSpell(7));
    }

    @Override
    public AbstractNPC construct(int id, Location location, Object... objects) {
        return new CatableponNPC(id, location);
    }

    @Override
    public CombatSwingHandler getSwingHandler(boolean swing) {
        return combatHandler;
    }

    @Override
    public int[] getIds() {
        return ID;
    }
}
