package content.region.misthalin.handlers.stronghold.dialogue;

import core.game.dialogue.Dialogue;
import core.game.node.entity.player.Player;
import core.plugin.Initializable;

/**
 * The Skull sceptre dialogue.
 */
@Initializable
public class SkullSceptreDialogue extends Dialogue {
    /**
     * Instantiates a new Skull sceptre dialogue.
     */
    public SkullSceptreDialogue() {
    }

    /**
     * Instantiates a new Skull sceptre dialogue.
     *
     * @param player the player
     */
    public SkullSceptreDialogue(Player player) {
        super(player);
    }

    @Override
    public int[] getIds() {
        return new int[]{78489};
    }

    @Override
    public boolean handle(int interfaceId, int buttonId) {
        end();
        return true;
    }

    @Override
    public Dialogue newInstance(Player player) {
        return new SkullSceptreDialogue(player);
    }

    @Override
    public boolean open(Object... args) {
        interpreter.sendItemMessage(9009, "The two halves of the skull fit perfectly.");
        if (args.length == 1) {
            interpreter.sendItemMessage(9012, "The two halves of the sceptre fit perfectly.");// The
            // Sceptre", "appears
            // to
            // be
            // designed
            // to
            // have
            // something
            // on
            // top.");
            return true;
        }
        if (args.length == 2) {
            interpreter.sendItemMessage(9013, "The skull fits perfectly atop the Sceptre.");// The
            // Sceptre", "appears
            // to
            // be
            // designed
            // to
            // have
            // something
            // on
            // top.");
            return true;
        }
        stage = 0;
        return true;
    }

}
