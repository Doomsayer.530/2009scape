package content.region.kandarin.handlers.khazard

import content.global.skill.support.construction.decoration.workshop.BrokenItem
import content.global.skill.support.construction.decoration.workshop.BrokenItem.getRepair
import content.region.kandarin.dialogue.TindelMerchantDialogue
import core.api.*
import core.api.consts.Items
import core.api.consts.NPCs
import core.api.consts.Scenery
import core.api.consts.Sounds
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.interaction.QueueStrength
import core.game.node.entity.npc.NPC
import core.game.node.item.Item
import core.game.world.map.Location
import core.tools.RandomFunction

class RustyExchangeListeners : InteractionListener {

    override fun defineListeners() {

        on(Scenery.ANTIQUES_SHOP_STALL_5831, IntType.SCENERY, "ring-bell") { player, _ ->
            playGlobalAudio(player.location, Sounds.BELL_2192)
            sendDialogue(player, "You ring for attention.")
            queueScript(player, 1, QueueStrength.SOFT) {
                openDialogue(player, TindelMerchantDialogue())
                return@queueScript stopExecuting(player)
            }
            return@on true
        }

        on(NPCs.TINDEL_MARCHANT_1799, IntType.NPC, "talk-to", "give-sword") { player, _ ->
            if (getUsedOption(player) == "talk-to") {
                openDialogue(player, TindelMerchantDialogue())
                return@on true
            }
            if (getUsedOption(player) == "give-sword") {
                if (!anyInInventory(player, Items.RUSTY_SWORD_686)) {
                    sendNPCDialogue(player, NPCs.TINDEL_MARCHANT_1799, "Sorry my friend, but you don't seem to have any swords that need to be identified..", FacialExpression.HALF_GUILTY)
                    return@on false
                }
                if (!inInventory(player, Items.COINS_995, 100)) {
                    sendNPCDialogue(player, NPCs.TINDEL_MARCHANT_1799, "Sorry, you don't have enough coins.", FacialExpression.HALF_GUILTY)
                    return@on false
                } else {
                    lock(player, 3)
                    lockInteractions(player, 3)
                    var rustySword = player.inventory.containsItem(Item(Items.RUSTY_SWORD_686))
                    openDialogue(player, object : DialogueFile() {
                        override fun handle(componentID: Int, buttonID: Int) {
                            npc = NPC(NPCs.TINDEL_MARCHANT_1799)
                            when (stage) {
                                0 -> {
                                    sendDoubleItemDialogue(player, Items.RUSTY_SWORD_686, Items.COINS_8896, "You hand Tindel 100 coins plus the ${getItemName(getRepair(
                                        BrokenItem.EquipmentType.SWORDS).id).lowercase()}.")
                                    stage++
                                }

                                1 -> {
                                    if (rustySword && RandomFunction.roll(2)) {
                                        removeItem(player, Item(Items.COINS_995, 100))
                                        removeItem(player, Item(Items.RUSTY_SWORD_686))
                                        npcl(FacialExpression.NEUTRAL, "There you go my friend, it turned out to be a ${getItemName(getRepair(
                                            BrokenItem.EquipmentType.SWORDS).id).lowercase()}.")
                                        stage++
                                    } else {
                                        end()
                                        unlock(player)
                                        npcl(FacialExpression.HALF_GUILTY, "Sorry my friend, but the item wasn't worth anything. I've disposed of it for you.")
                                    }
                                }

                                2 -> {
                                    end()
                                    unlock(player)
                                    player.inventory.add(getRepair(BrokenItem.EquipmentType.SWORDS))
                                    sendItemDialogue(player, getItemName(getRepair(BrokenItem.EquipmentType.SWORDS).id), "Tindel gives you a ${getItemName(getRepair(
                                        BrokenItem.EquipmentType.SWORDS).id).lowercase()}.")
                                }
                            }
                        }
                    })
                }
            }
            return@on true
        }
    }

    override fun defineDestinationOverrides() {
        setDest(IntType.NPC, intArrayOf(NPCs.TINDEL_MARCHANT_1799), "talk-to", "give-sword") { _, _ ->
            return@setDest Location(2678, 3152, 0)
        }
    }
}