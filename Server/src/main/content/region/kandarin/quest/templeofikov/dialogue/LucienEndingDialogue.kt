package content.region.kandarin.quest.templeofikov.dialogue

import core.api.*
import core.api.consts.Items
import core.api.consts.NPCs
import core.game.dialogue.Dialogue
import core.game.dialogue.DialogueBuilder
import core.game.dialogue.DialogueBuilderFile
import core.game.dialogue.FacialExpression
import core.game.node.entity.player.Player
import core.plugin.Initializable

@Initializable
class LucienEndingDialogue(player: Player? = null) : Dialogue(player) {

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        openDialogue(player, LucienEndingDialogueFile(), npc)
        return false
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.LUCIEN_272)
    }

}

class LucienEndingDialogueFile : DialogueBuilderFile() {
    override fun create(b: DialogueBuilder) {
        b.onQuestStages("Temple of Ikov", 100)
            .endWith { _, player ->
                sendMessage(player, "You have completed the Temple of Ikov quest.")
            }
        b.onQuestStages("Temple of Ikov", 1, 2, 3, 4, 5, 6, 7)
            .npcl(FacialExpression.FRIENDLY, "Have you got the Staff of Armadyl yet?")
            .branch { player ->
                return@branch if (inInventory(player, Items.STAFF_OF_ARMADYL_84)) {
                    1
                } else {
                    0
                }
            }.let { branch ->
                branch.onValue(1)
                    .options()
                    .let { optionBuilder ->
                        optionBuilder.option_playerl("Yes! Here it is.")
                            .betweenStage { _, player, _, _ ->
                                removeItem(player, Items.STAFF_OF_ARMADYL_84)
                            }
                            .iteml(Items.STAFF_OF_ARMADYL_84, "You give Lucien the Staff of Armadyl.")
                            .npcl(FacialExpression.FRIENDLY, "Muhahahhahahaha!")
                            .npcl(
                                FacialExpression.FRIENDLY,
                                "I can feel the power of the staff running through me! I will be more powerful and they shall bow down to me!"
                            )
                            .npcl(
                                FacialExpression.FRIENDLY,
                                "I suppose you want your reward? I shall grant you much power!"
                            )
                            .endWith { _, player ->
                                if (getQuestStage(player, "Temple of Ikov") == 6) {
                                    finishQuest(player, "Temple of Ikov")
                                }
                            }
                        optionBuilder.option_playerl("No, not yet.")
                            .end()
                    }
                branch.onValue(0)
                    .playerl(FacialExpression.FRIENDLY, "No, not yet.")
                    .end()
            }
        b.onPredicate { _ -> true }
            .npcl("Not here. Meet me at the Flying Horse Inn in East Ardougne.")
            .end()
    }
}