package content.region.kandarin.quest.waterfall.dialogue

import core.api.hasAnItem
import core.game.dialogue.Dialogue
import core.game.dialogue.DialogueInterpreter
import core.game.dialogue.FacialExpression
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable

class GolrieDialogue(player: Player? = null) : Dialogue(player) {

    override fun getIds(): IntArray {
        return intArrayOf(DialogueInterpreter.getDialogueKey("golrie_dialogue"), 306)
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            100 -> end()
            0 -> {
                interpreter.sendDialogues(
                    306,
                    FacialExpression.OLD_NORMAL,
                    "That's me. I've been stuck in here for weeks, those",
                    "goblins are trying to steal my family's heirlooms. My",
                    "grandad gave me all sorts of old junk."
                )
                stage = 1
            }

            1 -> {
                player(FacialExpression.ASKING, "Do you mind if I have a look?")
                stage = 2
            }

            2 -> {
                interpreter.sendDialogues(306, FacialExpression.OLD_HAPPY, "No, of course not.")
                stage = 3
            }

            3 -> {
                interpreter.sendDialogue("You look amongst the junk on the floor.")
                stage = if (hasAnItem(player, 294).container != null) {
                    50
                } else {
                    4
                }
            }

            4 -> {
                interpreter.sendDialogue("Mixed with the junk on the floor you find Glarial's pebble.")
                stage = 5
            }

            5 -> {
                player(FacialExpression.ASKING, "Could I take this old pebble?")
                stage = 6
            }

            6 -> {
                interpreter.sendDialogues(
                    306,
                    FacialExpression.OLD_NORMAL,
                    "Oh that, yes have it, it's just some old elven junk I",
                    "believe."
                )
                player.inventory.add(Item(294, 1))
                stage = 7
            }

            7 -> {
                interpreter.sendDialogue("You give the key to Golrie.")
                stage = 8
            }

            8 -> {
                interpreter.sendDialogues(
                    306,
                    FacialExpression.OLD_HAPPY,
                    "Thanks a lot for the key traveller. I think I'll wait in",
                    "here until those goblins get bored and leave."
                )
                player.inventory.remove(Item(293, 1))
                stage = 9
            }

            9 -> {
                player(FacialExpression.NEUTRAL, "OK... Take care Golrie.")
                stage = 100
            }

            50 -> {
                interpreter.sendDialogue("You find nothing of interest.")
                stage = 100
            }
        }
        return true
    }

    override fun open(vararg args: Any): Boolean {
        player(FacialExpression.NEUTRAL, "Hello, is your name Golrie?")
        stage = 0
        return true
    }
}