package content.region.kandarin.quest.thegrandtree.npc

import content.region.kandarin.quest.thegrandtree.dialogue.ForemanDialogueFile
import core.api.*
import core.api.consts.Items
import core.api.consts.NPCs
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.node.entity.Entity
import core.game.node.entity.npc.AbstractNPC
import core.game.node.entity.player.Player
import core.game.world.map.Location
import core.plugin.Initializable

@Initializable
class ForemanNPC(id: Int = 0, location: Location? = null) : AbstractNPC(id, location), InteractionListener {

    override fun construct(id: Int, location: Location?, vararg objects: Any?): AbstractNPC {
        return ForemanNPC(id, location)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.FOREMAN_674)
    }

    override fun defineListeners() {
        on(this.ids, IntType.NPC, "talk-to") { player, npc ->
            if(!isQuestComplete(player, "The Grand Tree")) {
                openDialogue(player, ForemanDialogueFile(), npc)
            } else {
                sendDialogue(player, "The foreman is too busy to talk.")
            }
            return@on true
        }
    }

    override fun finalizeDeath(killer: Entity?) {
        if (getQuestStage(killer as Player, "The Grand Tree") == 55) {
            sendMessage(killer, "The foreman drops a piece of paper as he dies.")
            produceGroundItem(killer, Items.LUMBER_ORDER_787, 1, this.location)
        }
        super.finalizeDeath(killer)
    }
}