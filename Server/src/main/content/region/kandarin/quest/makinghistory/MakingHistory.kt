package content.region.kandarin.quest.makinghistory

import content.global.activity.enchkey.EnchantedKeyHelper
import core.api.*
import core.api.consts.Components
import core.api.consts.Items
import core.api.consts.Vars
import core.game.node.entity.player.Player
import core.game.node.entity.player.link.quest.Quest
import core.game.node.entity.skill.Skills
import core.game.world.GameWorld
import core.plugin.Initializable

@Initializable
class MakingHistory : Quest("Making History", 86, 85, 3, Vars.VARBIT_QUEST_MAKING_HISTORY_PROGRESS, 0, 1, 4) {

    override fun drawJournal(player: Player?, stage: Int) {
        super.drawJournal(player, stage)
        var line = 11
        player ?: return
        if (stage == 0) {
            line(player, "I can start this quest by talking to !!Jorral?? in the !!Outpost??", line++, false)
            line(player, "!!North West of West Ardougne??.", line++, false)
            line += 1
            line(player, "Minimum requirements:", line++, false)
            line(player, "!!I must have completed the Priest in Peril Quest??", line++, isQuestComplete(player, "Priest in Peril"))
            line(player, "It will be easier with", line++, false)
            line(player, "!!Crafting lvl 24??", line++, getStatLevel(player, Skills.CRAFTING) >= 24)
            line(player, "!!Smithing lvl 40??", line++, getStatLevel(player, Skills.SMITHING) >= 40)
            line(player, "!!Mining lvl 40??", line++, getStatLevel(player, Skills.MINING) >= 40)
            line++
        }

        if (stage >= 1) {
            line(player, "Jorral wants to save the outpost from King Lathas plans.", line++)
            line++
        }

        if (getVarbit(player, MHUtils.PROGRESS) >= 3) {
            line(player, "I have gathered the parts of history. I have been given a letter.", line++)
            line++
        }

        if (stage >= 99) {
            line(player, "The king was pleased to see the history. He gave me a letter for Jorral", line++)
            line++
        }

        if (stage == 100) {
            line(player, "The outpost is saved!", line++, true)
            line++
            line(player, "<col=FF0000>QUEST COMPLETE!", line, false)
            line++
            if (getAttribute(player, EnchantedKeyHelper.ENCHANTED_KEY_ATTR, -1) >= 0) {
                line(player, "I Should see what else I can find with the help of the key.", line, getAttribute(player, EnchantedKeyHelper.ENCHANTED_KEY_ATTR, -1) >= 10)
            }
        }

    }

    override fun finish(player: Player?) {
        super.finish(player)
        player ?: return
        var ln = 10
        sendItemZoomOnInterface(player, Components.QUEST_COMPLETE_SCROLL_277, 5, Items.ENCHANTED_KEY_6754, 230)
        drawReward(player, "3 Quest points, 1000 Prayer", ln++)
        drawReward(player, "1000 Crafting XP, 750", ln++)
        drawReward(player, "gold coins. Use the enchanted", ln++)
        drawReward(player, "key all over " + GameWorld.settings!!.name + " Visit", ln++)
        drawReward(player, "the silver trader for help.", ln)
        rewardXP(player, Skills.CRAFTING, 1000.0)
        rewardXP(player, Skills.PRAYER, 1000.0)
        addItemOrDrop(player, Items.COINS_995, 750)
        setVarbit(player, 1390, 1, true)
        removeAttributes(player, MHUtils.ATTRIBUTE_ERIN_PROGRESS, MHUtils.ATTRIBUTE_DROALAK_PROGRESS, MHUtils.ATTRIBUTE_DRON_PROGRESS)
    }

    override fun newInstance(`object`: Any?): Quest {
        return this
    }

}