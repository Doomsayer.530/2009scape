package content.region.kandarin.dialogue

import core.api.*
import core.api.consts.NPCs
import core.game.dialogue.Dialogue
import core.game.dialogue.FacialExpression
import core.game.interaction.QueueStrength
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.world.update.flag.context.Animation
import core.plugin.Initializable
import core.tools.END_DIALOGUE

@Initializable
class GeraldDialogue(player: Player? = null) : Dialogue(player) {

    override fun open(vararg args: Any): Boolean {
        npc = args[0] as NPC
        if(getQuestStage(player, "Waterfall Quest") >= 1) {
            player("Hello.").also { stage = 5 }
        } else {
            player("Hello there.").also { stage = 0 }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> npcl(FacialExpression.NEUTRAL,"Good day to you traveller, are you here to fish or just looking around?").also { stage++ }
            1 -> npcl(FacialExpression.NEUTRAL,"I've caught some beauties down here.").also { stage++ }
            2 -> player("Really?").also { stage++ }
            3 -> npcl(FacialExpression.FRIENDLY, "The last one was this big!")
            4 -> {
                end()
                queueScript(player, 1, QueueStrength.WEAK) {
                    stopWalk(player)
                    animate(npc, Animation(1240))
                    return@queueScript stopExecuting(player)
                }
            }
            5 -> npcl(FacialExpression.NEUTRAL, "Hello traveller.").also { stage++ }
            6 -> npcl(FacialExpression.HALF_ASKING, "Are you here to fish or to hunt for treasure?").also { stage++ }
            7 -> player("Why do you say that?").also { stage++ }
            8 -> npcl(FacialExpression.NEUTRAL, "Adventurers pass through here every week, they never find anything though.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.GERALD_303)
    }

}