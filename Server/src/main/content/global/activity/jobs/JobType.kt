package content.global.activity.jobs

/**
 * @author Swizey
 * @date 14.02.2023
 */
enum class JobType {
    PRODUCTION, COMBAT, BONE_BURYING
}