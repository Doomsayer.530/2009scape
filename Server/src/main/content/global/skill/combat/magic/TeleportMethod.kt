package content.global.skill.combat.magic

enum class TeleportMethod {
    JEWELRY,
    SPELL,
    NPC
}