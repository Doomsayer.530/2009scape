package content.global.skill.combat.summoning;

/**
 * The enum Summoning scroll.
 */
public enum SummoningScroll {
    /**
     * Howl scroll summoning scroll.
     */
    HOWL_SCROLL(0, 12425, 0.1, 1, 12047),
    /**
     * Dreadfowl strike scroll summoning scroll.
     */
    DREADFOWL_STRIKE_SCROLL(1, 12445, 0.1, 4, 12043),
    /**
     * Egg spawn scroll summoning scroll.
     */
    EGG_SPAWN_SCROLL(2, 12428, 0.2, 10, 12059),
    /**
     * Slime spray scroll summoning scroll.
     */
    SLIME_SPRAY_SCROLL(3, 12459, 0.2, 13, 12019),
    /**
     * Stony shell scroll summoning scroll.
     */
    STONY_SHELL_SCROLL(4, 12533, 0.2, 16, 12009),
    /**
     * Pester scroll summoning scroll.
     */
    PESTER_SCROLL(5, 12838, 0.5, 17, 12778),
    /**
     * Electric lash scroll summoning scroll.
     */
    ELECTRIC_LASH_SCROLL(6, 12460, 0.4, 18, 12049),
    /**
     * Venom shot scroll summoning scroll.
     */
    VENOM_SHOT_SCROLL(7, 12432, 0.9, 19, 12055),
    /**
     * Fireball assault scroll summoning scroll.
     */
    FIREBALL_ASSAULT_SCROLL(8, 12839, 1.1, 22, 12808),
    /**
     * Cheese feast scroll summoning scroll.
     */
    CHEESE_FEAST_SCROLL(9, 12430, 2.3, 23, 12067),
    /**
     * Sandstorm scroll summoning scroll.
     */
    SANDSTORM_SCROLL(10, 12446, 2.5, 25, 12063),
    /**
     * Generate compost scroll summoning scroll.
     */
    GENERATE_COMPOST_SCROLL(11, 12440, 0.6, 28, 12091),
    /**
     * Explode scroll summoning scroll.
     */
    EXPLODE_SCROLL(12, 12834, 2.9, 29, 12800),
    /**
     * Vampyre touch scroll summoning scroll.
     */
    VAMPYRE_TOUCH_SCROLL(13, 12447, 1.5, 31, 12053),
    /**
     * Insane ferocity scroll summoning scroll.
     */
    INSANE_FEROCITY_SCROLL(14, 12433, 1.6, 32, 12065),
    /**
     * Multichop scroll summoning scroll.
     */
    MULTICHOP_SCROLL(15, 12429, 0.7, 33, 12021),
    /**
     * Call to arms scroll 1 summoning scroll.
     */
    CALL_TO_ARMS_SCROLL1(16, 12443, 0.7, 34, 12818),
    /**
     * Call to arms scroll 2 summoning scroll.
     */
    CALL_TO_ARMS_SCROLL2(17, 12443, 0.7, 34, 12814),
    /**
     * Call to arms scroll 3 summoning scroll.
     */
    CALL_TO_ARMS_SCROLL3(18, 12443, 0.7, 34, 12780),
    /**
     * Call to arms scroll 4 summoning scroll.
     */
    CALL_TO_ARMS_SCROLL4(19, 12443, 0.7, 34, 12798),
    /**
     * Bronze bull rush scroll summoning scroll.
     */
    BRONZE_BULL_RUSH_SCROLL(64, 12461, 3.6, 36, 12073),
    /**
     * Unburden scroll summoning scroll.
     */
    UNBURDEN_SCROLL(20, 12431, 0.6, 40, 12087),
    /**
     * Herbcall scroll summoning scroll.
     */
    HERBCALL_SCROLL(21, 12422, 0.8, 41, 12071),
    /**
     * Evil flames scroll summoning scroll.
     */
    EVIL_FLAMES_SCROLL(22, 12448, 2.1, 42, 12051),
    /**
     * Petrifying gaze scroll 1 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL1(23, 12458, 0.9, 43, 12095),
    /**
     * Petrifying gaze scroll 2 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL2(24, 12458, 0.9, 43, 12097),
    /**
     * Petrifying gaze scroll 3 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL3(25, 12458, 0.9, 43, 12099),
    /**
     * Petrifying gaze scroll 4 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL4(26, 12458, 0.9, 43, 12101),
    /**
     * Petrifying gaze scroll 5 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL5(27, 12458, 0.9, 43, 12103),
    /**
     * Petrifying gaze scroll 6 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL6(28, 12458, 0.9, 43, 12105),
    /**
     * Petrifying gaze scroll 7 summoning scroll.
     */
    PETRIFYING_GAZE_SCROLL7(29, 12458, 0.9, 43, 12107),
    /**
     * Iron bull rush scroll summoning scroll.
     */
    IRON_BULL_RUSH_SCROLL(65, 12462, 4.6, 46, 12075),
    /**
     * Immense heat scroll summoning scroll.
     */
    IMMENSE_HEAT_SCROLL(30, 12829, 2.3, 46, 12816),
    /**
     * Thieving fingers scroll summoning scroll.
     */
    THIEVING_FINGERS_SCROLL(31, 12426, 47, 47, 12041),
    /**
     * Blood drain scroll summoning scroll.
     */
    BLOOD_DRAIN_SCROLL(32, 12444, 2.4, 49, 12061),
    /**
     * Tireless run scroll summoning scroll.
     */
    TIRELESS_RUN_SCROLL(33, 12441, 0.8, 52, 12007),
    /**
     * Abyssal drain scroll summoning scroll.
     */
    ABYSSAL_DRAIN_SCROLL(34, 12454, 1.1, 54, 12035),
    /**
     * Dissolve scroll summoning scroll.
     */
    DISSOLVE_SCROLL(35, 12453, 5.5, 55, 12027),
    /**
     * Steel bull rush scroll summoning scroll.
     */
    STEEL_BULL_RUSH_SCROLL(66, 12463, 5.6, 56, 12077),
    /**
     * Fish rain scroll summoning scroll.
     */
    FISH_RAIN_SCROLL(36, 12424, 1.1, 56, 12531),
    /**
     * Ambush scroll summoning scroll.
     */
    AMBUSH_SCROLL(37, 12836, 5.7, 57, 12812),
    /**
     * Rending scroll summoning scroll.
     */
    RENDING_SCROLL(38, 12840, 5.7, 57, 12784),
    /**
     * Goad scroll summoning scroll.
     */
    GOAD_SCROLL(39, 12835, 5.7, 57, 12810),
    /**
     * Doomsphere scroll summoning scroll.
     */
    DOOMSPHERE_SCROLL(40, 12455, 5.8, 58, -1),
    /**
     * Dust cloud scroll summoning scroll.
     */
    DUST_CLOUD_SCROLL(41, 12468, 3.0, 61, 12085),
    /**
     * Abyssal stealth scroll summoning scroll.
     */
    ABYSSAL_STEALTH_SCROLL(42, 12427, 1.9, 62, 12037),
    /**
     * Ophidian incubation scroll summoning scroll.
     */
    OPHIDIAN_INCUBATION_SCROLL(43, 12436, 3.1, 63, 12015),
    /**
     * Poisonous blast scroll summoning scroll.
     */
    POISONOUS_BLAST_SCROLL(44, 12467, 3.2, 64, 12045),
    /**
     * Mithril bull rush scroll summoning scroll.
     */
    MITHRIL_BULL_RUSH_SCROLL(67, 12464, 6.6, 66, 12079),
    /**
     * Toad bark scroll summoning scroll.
     */
    TOAD_BARK_SCROLL(45, 12452, 1.0, 66, 12123),
    /**
     * Estudo scroll summoning scroll.
     */
    ESTUDO_SCROLL(46, 12439, 0.7, 67, 12031),
    /**
     * Swallow whole scroll summoning scroll.
     */
    SWALLOW_WHOLE_SCROLL(47, 12438, 1.4, 68, 12029),
    /**
     * Fruitfall scroll summoning scroll.
     */
    FRUITFALL_SCROLL(48, 12423, 1.4, 69, 12033),
    /**
     * Famine scroll summoning scroll.
     */
    FAMINE_SCROLL(49, 12830, 1.5, 70, 12820),
    /**
     * Arctic blast scroll summoning scroll.
     */
    ARCTIC_BLAST_SCROLL(50, 12451, 1.1, 71, 12057),

    /**
     * Volcanic strength scroll summoning scroll.
     */
// RISE_FROM_THE_ASHES_SCROLL(51, 14622, 8.0, 277, -1),
    VOLCANIC_STRENGTH_SCROLL(51, 12826, 7.3, 73, 12792),
    /**
     * Crushing claw scroll summoning scroll.
     */
    CRUSHING_CLAW_SCROLL(52, 12449, 3.7, 74, 12069),
    /**
     * Mantis strike scroll summoning scroll.
     */
    MANTIS_STRIKE_SCROLL(53, 12450, 3.7, 75, 12011),
    /**
     * Inferno scroll summoning scroll.
     */
    INFERNO_SCROLL(54, 12841, 1.5, 76, 12782),
    /**
     * Adamant bull rush scroll summoning scroll.
     */
    ADAMANT_BULL_RUSH_SCROLL(68, 12465, 7.6, 76, 12081),
    /**
     * Deadly claw scroll summoning scroll.
     */
    DEADLY_CLAW_SCROLL(55, 12831, 11.0, 77, 12162),
    /**
     * Acorn missile scroll summoning scroll.
     */
    ACORN_MISSILE_SCROLL(56, 12457, 1.6, 78, 12013),
    /**
     * Titans constitution scroll 1 summoning scroll.
     */
    TITANS_CONSTITUTION_SCROLL1(57, 12824, 7.9, 79, 12802),
    /**
     * Titans constitution scroll 2 summoning scroll.
     */
    TITANS_CONSTITUTION_SCROLL2(58, 12824, 7.9, 79, 12806),
    /**
     * Titans constitution scroll 3 summoning scroll.
     */
    TITANS_CONSTITUTION_SCROLL3(59, 12824, 7.9, 79, 12804),
    /**
     * Regrowth scroll summoning scroll.
     */
    REGROWTH_SCROLL(60, 12442, 1.6, 80, 12025),
    /**
     * Spike shot scroll summoning scroll.
     */
    SPIKE_SHOT_SCROLL(61, 12456, 4.1, 83, 12017),
    /**
     * Ebon thunder scroll summoning scroll.
     */
    EBON_THUNDER_SCROLL(62, 12837, 8.3, 83, 12788),
    /**
     * Swamp plague scroll summoning scroll.
     */
    SWAMP_PLAGUE_SCROLL(63, 12832, 4.1, 85, 12776),
    /**
     * Rune bull rush scroll summoning scroll.
     */
    RUNE_BULL_RUSH_SCROLL(69, 12466, 8.6, 86, 12083),
    /**
     * Healing aura scroll summoning scroll.
     */
    HEALING_AURA_SCROLL(70, 12434, 1.8, 88, 12039),
    /**
     * Boil scroll summoning scroll.
     */
    BOIL_SCROLL(71, 12833, 8.9, 89, 12786),
    /**
     * Magic focus scroll summoning scroll.
     */
    MAGIC_FOCUS_SCROLL(72, 12437, 4.6, 92, 12089),
    /**
     * Essence shipment scroll summoning scroll.
     */
    ESSENCE_SHIPMENT_SCROLL(73, 12827, 1.9, 93, 12796),
    /**
     * Iron within scroll summoning scroll.
     */
    IRON_WITHIN_SCROLL(74, 12828, 4.7, 95, 12822),
    /**
     * Winter storage scroll summoning scroll.
     */
    WINTER_STORAGE_SCROLL(75, 12435, 4.8, 96, 12093),
    /**
     * Steel of legends scroll summoning scroll.
     */
    STEEL_OF_LEGENDS_SCROLL(76, 12825, 4.9, 99, 12790),
    /**
     * Clay deposit scroll 1 summoning scroll.
     */
    CLAY_DEPOSIT_SCROLL_1(-1, 14421, 0, 1, 14422),
    /**
     * Clay deposit scroll 2 summoning scroll.
     */
    CLAY_DEPOSIT_SCROLL_2(-1, 14421, 0, 20, 14424),
    /**
     * Clay deposit scroll 3 summoning scroll.
     */
    CLAY_DEPOSIT_SCROLL_3(-1, 14421, 0, 40, 14426),
    /**
     * Clay deposit scroll 4 summoning scroll.
     */
    CLAY_DEPOSIT_SCROLL_4(-1, 14421, 0, 60, 14428),
    /**
     * Clay deposit scroll 5 summoning scroll.
     */
    CLAY_DEPOSIT_SCROLL_5(-1, 14421, 0, 80, 14430);

    private final int level;

    private final int itemId;

    private final int slotId;

    private final double xp;

    private final int[] items;


    SummoningScroll(int slotId, int itemId, double xp, int level, int... items) {
        this.level = level;
        this.itemId = itemId;
        this.xp = xp;
        this.slotId = slotId;
        this.items = items;
    }

    /**
     * Gets pouch.
     *
     * @return the pouch
     */
    public int getPouch() {
        return items[0];
    }

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets item id.
     *
     * @return the item id
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * Gets slot id.
     *
     * @return the slot id
     */
    public int getSlotId() {
        return slotId;
    }

    /**
     * Gets experience.
     *
     * @return the experience
     */
    public double getExperience() {
        return xp;
    }

    /**
     * Get items int [ ].
     *
     * @return the int [ ]
     */
    public int[] getItems() {
        return items;
    }

    /**
     * For id summoning scroll.
     *
     * @param id the id
     * @return the summoning scroll
     */
    public static SummoningScroll forId(int id) {
        for (SummoningScroll scroll : SummoningScroll.values()) {
            if (scroll.slotId == id) {
                return scroll;
            }
        }
        return null;
    }

    /**
     * For item id summoning scroll.
     *
     * @param id the id
     * @return the summoning scroll
     */
    public static SummoningScroll forItemId(int id) {
        for (SummoningScroll scroll : SummoningScroll.values()) {
            if (scroll.itemId == id) {
                return scroll;
            }
        }
        return null;
    }

    /**
     * For pouch summoning scroll.
     *
     * @param pouchId the pouch id
     * @return the summoning scroll
     */
    public static SummoningScroll forPouch(int pouchId) {
        for (SummoningScroll scroll : SummoningScroll.values()) {
            if (scroll.items[0] == pouchId) {
                return scroll;
            }
        }
        return null;
    }

}
