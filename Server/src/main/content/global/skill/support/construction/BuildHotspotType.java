package content.global.skill.support.construction;

/**
 * The enum Build hotspot type.
 */
public enum BuildHotspotType {

    /**
     * Individual build hotspot type.
     */
    INDIVIDUAL,
    /**
     * Recursive build hotspot type.
     */
    RECURSIVE,
    /**
     * Linked build hotspot type.
     */
    LINKED,
    /**
     * Crest build hotspot type.
     */
    CREST,
    /**
     * Staircase build hotspot type.
     */
    STAIRCASE;
	
	BuildHotspotType() {
		
	}

} 
