package content.global.skill

object BarbarianTraining {

    // Base attribute.
    val ATTR_BARB_TRAIN_START = "barbtraining:start"

    // Completed every training in each skill, [NOT USED].
    val ATTR_BARB_TRAIN_FIREMAKING_COMPLETED = "barbtraining:firemaking-complete" // NOT USED
    val ATTR_BARB_TRAIN_FISHING_COMPLETED = "barbtraining:fishing-complete" // NOT USED
    val ATTR_BARB_TRAIN_HERBLORE_COMPLETED = "barbtraining:herblore-complete" // NOT USED
    val ATTR_BARB_TRAIN_SMITHING_COMPLETED = "barbtraining:smithing-complete" // NOT USED

    // Fishing
    val ATTR_BARB_TRAIN_FISHING_BEGIN = "barbtraining:fishing-start"
    val ATTR_BARB_TRAIN_FISHING = "barbtraining:fishing"
    val ATTR_BARB_TRAIN_FISHING_BAREHAND = "barbtraining:barehand" // NOT USED

    // Firemaking
    val ATTR_BARB_TRAIN_FIREMAKING_BEGIN = "barbtraining:firemaking-start"
    val ATTR_BARB_TRAIN_FIREMAKING = "barbtraining:firemaking"
    val ATTR_BARB_TRAIN_PYRE_SHIP = "barbtraining:ancients" // NOT USED

    // Herblore
    val ATTR_BARB_TRAIN_HERBLORE_BEGIN = "barbtraining:herblore-start"
    val ATTR_BARB_TRAIN_HERBLORE = "barbtraining:herblore"

    // Smithing
    val ATTR_BARB_TRAIN_SMITHING_BEGIN = "barbtraining:smithing-start" // NOT USED
    val ATTR_BARB_TRAIN_SMITHING_SPEAR = "barbtraining:smithing-spear" // NOT USED
    val ATTR_BARB_TRAIN_SMITHING_HASTA = "barbtraining:smithing-hasta" // NOT USED

}