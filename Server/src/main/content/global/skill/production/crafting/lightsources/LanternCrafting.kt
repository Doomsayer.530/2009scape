package content.global.skill.production.crafting.lightsources

import core.api.addItem
import core.api.consts.Items
import core.api.getStatLevel
import core.api.removeItem
import core.api.sendMessage
import core.game.interaction.NodeUsageEvent
import core.game.interaction.UseWithHandler
import core.game.node.entity.player.Player
import core.game.node.entity.skill.Skills
import core.plugin.Initializable
import core.plugin.Plugin

@Initializable
class LanternCrafting : UseWithHandler(36, 38, 4525, 4542, 1607) {

    override fun newInstance(arg: Any?): Plugin<Any> {
        addHandler(Items.CANDLE_LANTERN_4527, ITEM_TYPE, this)
        addHandler(Items.OIL_LANTERN_FRAME_4540, ITEM_TYPE, this)
        addHandler(Items.BULLSEYE_LANTERN_4544, ITEM_TYPE, this)
        return this
    }

    override fun handle(event: NodeUsageEvent?): Boolean {
        event ?: return false
        val used = event.used
        return when (used.id) {
            Items.CANDLE_LANTERN_4527 -> craftCandleLantern(event.player, event)
            Items.OIL_LANTERN_FRAME_4540 -> craftOilLantern(event.player, event)
            Items.BULLSEYE_LANTERN_4544 -> craftBullseyeLantern(event.player, event)
            else -> false
        }
    }

    private fun craftCandleLantern(player: Player, event: NodeUsageEvent): Boolean {
        return when (event.usedWith.id) {
            36, 38 -> {
                removeEventItems(player, event)
                addItem(player, if (event.usedWith.id == 36) Items.CANDLE_LANTERN_4529 else Items.CANDLE_LANTERN_4532)
                sendMessage(player, "You place the unlit candle inside the lantern.")
                true
            }

            else -> false
        }
    }

    private fun craftOilLantern(player: Player, event: NodeUsageEvent): Boolean {
        return when (event.usedWith.id) {
            4525 -> {
                removeEventItems(player, event)
                addItem(player, Items.OIL_LANTERN_4535)
                sendMessage(player, "You place the oil lamp inside its metal frame.")
                true
            }

            else -> false
        }
    }

    private fun craftBullseyeLantern(player: Player, event: NodeUsageEvent): Boolean {
        return when (event.usedWith.id) {
            4542 -> {
                removeEventItems(player, event)
                addItem(player, Items.BULLSEYE_LANTERN_4546)
                sendMessage(player, "You fashion the lens onto the lantern.")
                true
            }

            1607 -> {
                if (getStatLevel(player, Skills.CRAFTING) < 20) {
                    sendMessage(player, "You require a crafting level of 20 to use a gem as a lens.")
                } else {
                    removeEventItems(player, event)
                    addItem(player, Items.SAPPHIRE_LANTERN_4700)
                    sendMessage(player, "You fashion the gem into a lens and fit it onto the lantern.")
                }
                true
            }

            else -> false
        }
    }

    private fun removeEventItems(player: Player, event: NodeUsageEvent) {
        removeItem(player, event.used.asItem())
        removeItem(player, event.usedWith.asItem())
    }
}