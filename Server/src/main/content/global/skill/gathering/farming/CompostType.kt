package content.global.skill.gathering.farming

enum class CompostType {
    NONE,
    COMPOST,
    SUPERCOMPOST
}