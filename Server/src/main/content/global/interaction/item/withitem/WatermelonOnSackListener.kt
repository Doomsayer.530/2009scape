package content.global.interaction.item.withitem

import core.api.*
import core.api.consts.Items
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.node.entity.skill.Skills

class WatermelonOnSackListener : InteractionListener {

    private val haySack = Items.HAY_SACK_6058
    private val watermelon = Items.WATERMELON_5982
    private val scarecrow = Items.SCARECROW_6059

    override fun defineListeners() {
        onUseWith(IntType.ITEM, haySack, watermelon) { player, _, _ ->
            if (getStatLevel(player, Skills.FARMING) < 23) {
                removeItem(player, haySack)
                removeItem(player, watermelon)
                addItem(player, scarecrow)
                rewardXP(player, Skills.FARMING, 25.0)
                sendMessage(player, "You stab the watermelon on top of the spear, finishing your scarecrow")
            } else {
                sendMessage(player, "Your Farming level is not high enough to do this")
            }
            return@onUseWith true
        }
    }
}