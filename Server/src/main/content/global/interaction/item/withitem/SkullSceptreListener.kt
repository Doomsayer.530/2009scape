package content.global.interaction.item.withitem

import core.api.*
import core.api.consts.Animations
import core.api.consts.Items
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.interaction.QueueStrength
import core.game.node.entity.player.Player
import core.game.world.map.Location
import core.game.world.update.flag.context.Graphic

class SkullSceptreListener : InteractionListener {

    private val skullSceptre = Items.SKULL_SCEPTRE_9013

    override fun defineListeners() {
        on(skullSceptre, IntType.ITEM, "invoke", "divine", "operate") { player, node ->
            when (getUsedOption(player)) {
                "invoke" -> {
                    if (inInventory(player, skullSceptre, 1)) {
                        sceptreTeleport(player)
                    }
                    setCharge(node, getCharge(node) - 200)
                    if (getCharge(node) < 1) {
                        removeItem(player, skullSceptre, Container.INVENTORY)
                        sendMessage(player, "Your staff crumbles to dust as you use its last charge.")
                    }
                }

                "divine" -> {
                    if (getCharge(node) < 1) {
                        sendMessage(player, "You don't have enough charges left.")
                    }
                    sendMessage(player, "Concentrating deeply, you divine that the sceptre has " + (getCharge(node) / 200) + " charges left.")
                }

                "operate" -> {
                    if (inEquipment(player, skullSceptre, 1)) {
                        sceptreTeleport(player)
                    }
                    setCharge(node, getCharge(node) - 200)
                    if (getCharge(node) < 1) {
                        removeItem(player, skullSceptre, Container.EQUIPMENT)
                        sendMessage(player, "Your staff crumbles to dust as you use its last charge.")
                    }
                }
            }
            return@on true
        }
    }

    private fun sceptreTeleport(player: Player) {
        lock(player, 4)
        visualize(player, Animations.HUMAN_USE_SCEPTRE_9601,
            Graphic(1683, 100)
        )
        queueScript(player, 3, QueueStrength.SOFT) {
            teleport(player, Location(3081, 3421, 0))
            return@queueScript stopExecuting(player)
        }
    }
}