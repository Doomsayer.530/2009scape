package content.global.interaction.item.withnpc

import content.region.misc.dialogue.apeatoll.dungeon.ZooknockDialogueFile
import core.api.consts.Items
import core.api.consts.NPCs
import core.api.openDialogue
import core.game.interaction.IntType
import core.game.interaction.InteractionListener


open class ZooknockEnchantBarsListener : InteractionListener {

    val goldBar = Items.GOLD_BAR_2357
    val monkeyAmuletMould = Items.MAMULET_MOULD_4020
    val monkeyDentures = Items.MONKEY_DENTURES_4006
    val items = intArrayOf(goldBar, monkeyDentures, monkeyAmuletMould)

    val zooknock = NPCs.ZOOKNOCK_1425

    override fun defineListeners() {
        onUseWith(IntType.NPC, items, zooknock) { player, used, _ ->
            openDialogue(player, ZooknockDialogueFile(used.id))
            return@onUseWith false
        }
    }
}