package content.global.interaction.scenery

import core.api.consts.Scenery
import core.api.replaceScenery
import core.game.interaction.IntType
import core.game.interaction.InteractionListener

class ChestSceneryListener : InteractionListener {

    override fun defineListeners() {

        /*
            Zanaris chests (near fairy bank)
         */

        on(Scenery.OPEN_CHEST_12121, IntType.SCENERY, "shut") { _, node ->
            replaceScenery(node.asScenery(), Scenery.CLOSED_CHEST_12120, -1)
            return@on true
        }

        on(Scenery.CLOSED_CHEST_12120, IntType.SCENERY, "open") { _, node ->
            replaceScenery(node.asScenery(), Scenery.OPEN_CHEST_12121, -1)
            return@on true
        }
    }
}