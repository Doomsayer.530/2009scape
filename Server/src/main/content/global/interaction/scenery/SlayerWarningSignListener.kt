package content.global.interaction.scenery

import core.api.consts.Scenery
import core.api.sendDialogueLines
import core.game.interaction.IntType
import core.game.interaction.InteractionListener

class SlayerWarningSignListener : InteractionListener {
    override fun defineListeners() {
        on(Scenery.DANGER_SIGN_5127, IntType.SCENERY, "read") { player, _ ->
            sendDialogueLines(player, "<col=FFF0000>WARNING!</col>","This area contains very dangerous creatures!","Do not pass unless properly prepared!")
            return@on true
        }
    }
}