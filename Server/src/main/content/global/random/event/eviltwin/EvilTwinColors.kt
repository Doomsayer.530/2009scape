package content.global.random.event.eviltwin

enum class EvilTwinColors {
    RED,
    GREEN,
    BLUE,
    YELLOW
}