package core.cache.def.impl;

/**
 * The type Linked scripts.
 */
public class LinkedScripts {
    /**
     * The Unknown.
     */
    public ScriptArgs unknown;
    /**
     * The On mouse over.
     */
    public ScriptArgs onMouseOver;
    /**
     * The On mouse leave.
     */
    public ScriptArgs onMouseLeave;
    /**
     * The On use with.
     */
    public ScriptArgs onUseWith;
    /**
     * The On use.
     */
    public ScriptArgs onUse;
    /**
     * The On varp transmit.
     */
    public ScriptArgs onVarpTransmit;
    /**
     * The On inv transmit.
     */
    public ScriptArgs onInvTransmit;
    /**
     * The On stat transmit.
     */
    public ScriptArgs onStatTransmit;
    /**
     * The On timer.
     */
    public ScriptArgs onTimer;
    /**
     * The On option click.
     */
    public ScriptArgs onOptionClick;
    /**
     * The On mouse repeat.
     */
    public ScriptArgs onMouseRepeat;
    /**
     * The On click repeat.
     */
    public ScriptArgs onClickRepeat;
    /**
     * The On drag.
     */
    public ScriptArgs onDrag;
    /**
     * The On release.
     */
    public ScriptArgs onRelease;
    /**
     * The On hold.
     */
    public ScriptArgs onHold;
    /**
     * The On drag start.
     */
    public ScriptArgs onDragStart;
    /**
     * The On drag release.
     */
    public ScriptArgs onDragRelease;
    /**
     * The On scroll.
     */
    public ScriptArgs onScroll;
    /**
     * The On varc transmit.
     */
    public ScriptArgs onVarcTransmit;
    /**
     * The On varcstr transmit.
     */
    public ScriptArgs onVarcstrTransmit;
}
