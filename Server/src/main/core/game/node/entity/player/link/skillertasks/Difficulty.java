package core.game.node.entity.player.link.skillertasks;

/**
 * The enum Difficulty.
 */
public enum Difficulty {
    /**
     * Novice difficulty.
     */
    NOVICE,
    /**
     * Intermediate difficulty.
     */
    INTERMEDIATE,
    /**
     * Advanced difficulty.
     */
    ADVANCED,
    /**
     * Elite difficulty.
     */
    ELITE
}