package core.game.node.entity.player.link.skillertasks;

import core.game.node.entity.skill.Skills;

/**
 * The enum Skill tasks.
 */
public enum SkillTasks {

    /**
     * The Fanchovies 1.
     */
// Fishing
	FANCHOVIES1("Fish Anchovies Novice", "You must successfully catch 230 Raw Anchovies.", Difficulty.NOVICE, 230, Skills.FISHING, 15),
    /**
     * The Fanchovies 2.
     */
    FANCHOVIES2("Fish Anchovies Intermediate", "You must successfully catch 480 Raw Anchovies.", Difficulty.INTERMEDIATE, 480, Skills.FISHING, 15),
    /**
     * The Fherring 1.
     */
    FHERRING1("Fish Herring Novice", "You must successfully catch 280 Raw Herring.", Difficulty.NOVICE, 280, Skills.FISHING, 10),
    /**
     * The Fherring 2.
     */
    FHERRING2("Fish Herring Intermediate", "You must successfully catch 450 Raw Herring.", Difficulty.INTERMEDIATE, 450, Skills.FISHING, 10),
    /**
     * The Flobster 1.
     */
    FLOBSTER1("Fish Lobster Intermediate", "You must successfully catch 300 Raw Lobster.", Difficulty.INTERMEDIATE, 300, Skills.FISHING, 40),
    /**
     * The Flobster 2.
     */
    FLOBSTER2("Fish Lobster Advanced", "You must successfully catch 650 Raw Lobster.", Difficulty.ADVANCED, 650, Skills.FISHING, 40),
    /**
     * The Fsalmon 1.
     */
    FSALMON1("Fish Salmon Novice", "You must successfully catch 220 Raw Salmon.", Difficulty.NOVICE, 220, Skills.FISHING, 30),
    /**
     * The Fsalmon 2.
     */
    FSALMON2("Fish Salmon Intermediate", "You must successfully catch 400 Raw Salmon.", Difficulty.INTERMEDIATE, 400, Skills.FISHING, 30),
    /**
     * The Fshark 1.
     */
    FSHARK1("Fish Shark Intermediate", "You must successfully catch 220 Raw Shark.", Difficulty.INTERMEDIATE, 220, Skills.FISHING, 76),
    /**
     * The Fshark 2.
     */
    FSHARK2("Fish Shark Advanced", "You must successfully catch 380 Raw Shark.", Difficulty.ADVANCED, 380, Skills.FISHING, 76),
    /**
     * The Fshark 3.
     */
    FSHARK3("Fish Shark Elite", "You must successfully catch 500 Raw Shark.", Difficulty.ELITE, 500, Skills.FISHING, 76),
    /**
     * The Fshrimp 1.
     */
    FSHRIMP1("Fish Shrimp Novice", "You must successfully catch 300 Raw Shrimp.", Difficulty.NOVICE, 300, Skills.FISHING, 1),
    /**
     * The Fshrimp 2.
     */
    FSHRIMP2("Fish Shrimp Intermediate", "You must successfully catch 500 Raw Shrimp.", Difficulty.INTERMEDIATE, 500, Skills.FISHING, 1),
    /**
     * The Fsword 1.
     */
    FSWORD1("Fish Swordfish Intermediate", "You must successfully catch 320 Raw Swordfish.", Difficulty.INTERMEDIATE, 320, Skills.FISHING, 50),
    /**
     * The Fsword 2.
     */
    FSWORD2("Fish Swordfish Advanced", "You must successfully catch 510 Raw Swordfish.", Difficulty.ADVANCED, 510, Skills.FISHING, 50),
    /**
     * The Ftrout 1.
     */
    FTROUT1("Fish Trout Novice", "You must successfully catch 250 Raw Trout.", Difficulty.NOVICE, 250, Skills.FISHING, 20),
    /**
     * The Ftrout 2.
     */
    FTROUT2("Fish Trout Intermediate", "You must successfully catch 420 Raw Trout.", Difficulty.INTERMEDIATE, 420, Skills.FISHING, 20),
    /**
     * The Ftuna 1.
     */
    FTUNA1("Fish Tuna Novice", "You must successfully catch 150 Raw Tuna.", Difficulty.NOVICE, 150, Skills.FISHING, 35),
    /**
     * The Ftuna 2.
     */
    FTUNA2("Fish Tuna Intermediate", "You must successfully catch 340 Raw Tuna.", Difficulty.INTERMEDIATE, 340, Skills.FISHING, 35);

	private String assignment;
	private String description;
	private Difficulty difficulty;
	private int amount;
	private int skill;
	private int level;

	SkillTasks(String assignment, String description, Difficulty difficulty, int amount, int skill, int level) {
		this.assignment = assignment;
		this.description = description;
		this.difficulty = difficulty;
		this.amount = amount;
		this.skill = skill;
		this.level = level;
	}

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public int getAmount() {
		return amount;
	}

    /**
     * Gets assignment.
     *
     * @return the assignment
     */
    public String getAssignment() {
		return assignment;
	}

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
		return description;
	}

    /**
     * Gets difficulty.
     *
     * @return the difficulty
     */
    public Difficulty getDifficulty() {
		return difficulty;
	}

    /**
     * Gets level.
     *
     * @return the level
     */
    public int getLevel() {
		return level;
	}

    /**
     * Gets skill.
     *
     * @return the skill
     */
    public int getSkill() {
		return skill;
	}

}
