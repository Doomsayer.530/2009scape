package core.game.node.entity.player.link.skillertasks;

import core.game.node.entity.player.Player;
import core.game.node.item.Item;
import core.tools.RandomFunction;

import java.util.ArrayList;

/**
 * The type Skiller tasks.
 */
public class SkillerTasks {

    /**
     * The Task.
     */
    public SkillTasks task;
    /**
     * The Task amount.
     */
    public int taskAmount;

    /**
     * Decrease task.
     *
     * @param player the player
     * @param stask  the stask
     */
    public void decreaseTask(Player player, SkillTasks stask) {
		player.getGlobalData().setTaskAmount(taskAmount);
		if (!hasTask() || taskAmount == 0 || !task.getAssignment().equalsIgnoreCase(stask.getAssignment())) {
			return;
		}
		taskAmount--;
		if (taskAmount == 0) {
			if (player.getSkillTasks().getCurrentTask().getDifficulty() == Difficulty.NOVICE) {
				player.getGlobalData().setTaskPoints(player.getGlobalData().getTaskPoints() + 1);
				player.getInventory().add(new Item(995, 10000));
			} else if (player.getSkillTasks().getCurrentTask().getDifficulty() == Difficulty.INTERMEDIATE) {
				player.getGlobalData().setTaskPoints(player.getGlobalData().getTaskPoints() + 3);
				player.getInventory().add(new Item(995, 30000));
			} else if (player.getSkillTasks().getCurrentTask().getDifficulty() ==Difficulty.ADVANCED) {
				player.getGlobalData().setTaskPoints(player.getGlobalData().getTaskPoints() + 7);
				player.getInventory().add(new Item(995, 70000));
			} else if (player.getSkillTasks().getCurrentTask().getDifficulty() ==Difficulty.ELITE) {
				player.getGlobalData().setTaskPoints(player.getGlobalData().getTaskPoints() + 10);
				player.getInventory().add(new Item(995, 100000));
			}
			player.getSkillTasks().setCurrentTask(null);
			player.sendMessage("<col=00FFFF>Your task has been completed! Return to Jack for another assignment!</col>");
			return;
		}
	}

    /**
     * Gets new task.
     *
     * @param player the player
     * @param tier   the tier
     * @return the new task
     */
    public SkillTasks getNewTask(Player player, Difficulty tier) {
		final ArrayList<SkillTasks> tasks = new ArrayList<SkillTasks>();
		for (final SkillTasks t : SkillTasks.values()) {
			if (player.getSkills().getExperienceByLevel(t.getSkill()) < t.getLevel()) {
				continue;
			}
			if (t.getDifficulty() == tier)
				tasks.add(t);
		}
		setCurrentTask(tasks.get(RandomFunction.random(tasks.size() - 1)));
		setTaskAmount(task.getAmount());
		return task;
	}

    /**
     * Gets current task.
     *
     * @return the current task
     */
    public SkillTasks getCurrentTask() {
		return task;
	}

    /**
     * Gets task amount.
     *
     * @return the task amount
     */
    public int getTaskAmount() {
		return taskAmount;
	}

    /**
     * Has task boolean.
     *
     * @return the boolean
     */
    public boolean hasTask() {
		return task != null;
	}

    /**
     * Is completed boolean.
     *
     * @return the boolean
     */
    public boolean isCompleted() {
		return hasTask() && taskAmount == 0;
	}

    /**
     * Sets current task.
     *
     * @param task the task
     */
    public void setCurrentTask(SkillTasks task) {
		this.task = task;
	}

    /**
     * Sets task amount.
     *
     * @param amount the amount
     */
    public void setTaskAmount(int amount) {
		this.taskAmount = amount;
	}


}
