package core.game.global.action;

import core.game.node.entity.player.Player;
import core.game.node.entity.player.link.diary.DiaryType;
import core.game.world.map.Location;

import java.util.Arrays;
import java.util.HashMap;

/**
 * The enum Special ladders.
 */
public enum SpecialLadders implements LadderAchievementCheck {

    /**
     * Alkharid crafting down special ladders.
     */
    ALKHARID_CRAFTING_DOWN(Location.create(3314, 3187, 1), Location.create(3310, 3187, 0)),
    /**
     * Alkharid crafting up special ladders.
     */
    ALKHARID_CRAFTING_UP(Location.create(3311, 3187, 0), Location.create(3314, 3187, 1)),
    /**
     * Alkharid socrceress down special ladders.
     */
    ALKHARID_SOCRCERESS_DOWN(Location.create(3325, 3139, 1), Location.create(3325, 3143, 0)),
    /**
     * Alkharid socrceress up special ladders.
     */
    ALKHARID_SOCRCERESS_UP(Location.create(3325, 3142, 0), Location.create(3325, 3139, 1)),
    /**
     * Alkharid zeke down special ladders.
     */
    ALKHARID_ZEKE_DOWN(Location.create(3284, 3190, 1), Location.create(3284, 3186, 0)),
    /**
     * Alkharid zeke up special ladders.
     */
    ALKHARID_ZEKE_UP(Location.create(3284, 3186, 0), Location.create(3284, 3190, 1)),
    /**
     * Bear cage down special ladders.
     */
    BEAR_CAGE_DOWN(Location.create(3230, 3508, 0), Location.create(3229, 9904, 0)),
    /**
     * Bear cage up special ladders.
     */
    BEAR_CAGE_UP(Location.create(3230, 9904, 0), Location.create(3231, 3504, 0)),
    /**
     * The Catherby black dragons ladder up.
     */
    CATHERBY_BLACK_DRAGONS_LADDER_UP(new Location(2841, 9824, 0), new Location(2842, 3423, 0)),
    /**
     * The Traverley dungeon ladder up.
     */
    TRAVERLEY_DUNGEON_LADDER_UP(new Location(2884, 9797, 0), new Location(2884, 3398, 0)),
    /**
     * Castlewars saradomin main floor stairs down special ladders.
     */
    CASTLEWARS_SARADOMIN_MAIN_FLOOR_STAIRS_DOWN(Location.create(2419, 3080, 1), Location.create(2419, 3077, 0)),
    /**
     * Castlewars saradomin main floor stairs up special ladders.
     */
    CASTLEWARS_SARADOMIN_MAIN_FLOOR_STAIRS_UP(Location.create(2428, 3081, 1), Location.create(2430, 3080, 2)),
    /**
     * Castlewars saradomin outer wall stairs down special ladders.
     */
    CASTLEWARS_SARADOMIN_OUTER_WALL_STAIRS_DOWN(Location.create(2417, 3075, 0), Location.create(2417, 3078, 0)),
    /**
     * Castlewars saradomin outer wall stairs up special ladders.
     */
    CASTLEWARS_SARADOMIN_OUTER_WALL_STAIRS_UP(Location.create(2417, 3077, 0), Location.create(2416, 3075, 0)),
    /**
     * Castlewars zamorak main floor stairs up special ladders.
     */
    CASTLEWARS_ZAMORAK_MAIN_FLOOR_STAIRS_UP(Location.create(2380, 3129, 0), Location.create(2379, 3127, 1)),
    /**
     * Castlewars zamorak outerwall stairs up special ladders.
     */
    CASTLEWARS_ZAMORAK_OUTERWALL_STAIRS_UP(Location.create(2382, 3130, 0), Location.create(2383, 3132, 0)),
    /**
     * Castlewars zamorak top floor down special ladders.
     */
    CASTLEWARS_ZAMORAK_TOP_FLOOR_DOWN(Location.create(2374, 3133, 3), Location.create(2374, 3130, 2)),
    /**
     * Castlewars zamouter wall stairs down special ladders.
     */
    CASTLEWARS_ZAMOUTER_WALL_STAIRS_DOWN(Location.create(2382, 3132, 0), Location.create(2382, 3129, 0)),
    /**
     * Clocktower hidden ladder special ladders.
     */
    CLOCKTOWER_HIDDEN_LADDER(Location.create(2572, 9631, 0), Location.create(2572, 3230, 0)),
    /**
     * The Draynor sewer northwest down.
     */
    DRAYNOR_SEWER_NORTHWEST_DOWN(new Location(3084, 3272, 0), new Location(3085, 9672, 0)),
    /**
     * The Draynor sewer northwest up.
     */
    DRAYNOR_SEWER_NORTHWEST_UP(new Location(3084, 9672, 0), new Location(3084, 3271, 0)),
    /**
     * The Draynor sewer southeast down.
     */
    DRAYNOR_SEWER_SOUTHEAST_DOWN(new Location(3118, 3244, 0), new Location(3118, 9643, 0)),
    /**
     * The Draynor sewer southeast up.
     */
    DRAYNOR_SEWER_SOUTHEAST_UP(new Location(3118, 9643, 0), new Location(3118, 3243, 0)),
    /**
     * The Entrana glassblowing pipe house down.
     */
    ENTRANA_GLASSBLOWING_PIPE_HOUSE_DOWN(new Location(2816, 3352, 1), new Location(2817, 3352, 0)),
    /**
     * The Edgeville climb up ladders.
     */
    EDGEVILLE_CLIMB_UP(new Location(3097, 9867, 0), new Location(3096, 3468, 0)),
    /**
     * Fog enter special ladders.
     */
    FOG_ENTER(Location.create(3240, 3575, 0), Location.create(1675, 5599, 0)),
    /**
     * Fog leave special ladders.
     */
    FOG_LEAVE(Location.create(1673, 5598, 0), Location.create(3242, 3574, 0)),
    /**
     * The Gem mine.
     */
    GEM_MINE(new Location(2821, 2996, 0), new Location(2838, 9387, 0)),
    /**
     * The Gem mine up.
     */
    GEM_MINE_UP(new Location(2838, 9388, 0), new Location(2820, 2996, 0)),
    /**
     * Glarial exit special ladders.
     */
    GLARIAL_EXIT(Location.create(2556, 9844, 0), Location.create(2557, 3444, 0)),
    /**
     * Hadley house stairscase up special ladders.
     */
    HADLEY_HOUSE_STAIRSCASE_UP(Location.create(2517, 3429, 0), Location.create(2518, 3431, 1)),
    /**
     * The Ham storage up.
     */
    HAM_STORAGE_UP(new Location(2567, 5185, 0), new Location(3166, 9623, 0)),
    /**
     * The Heroes guild ladder up.
     */
    HEROES_GUILD_LADDER_UP(new Location(2890, 3508, 1), new Location(2890, 3507, 2)),
    /**
     * The Heroes guild staircase up.
     */
    HEROES_GUILD_STAIRCASE_UP(new Location(2895, 3513, 0), new Location(2897, 3513, 1)),
    /**
     * The Horror from the deep basement up.
     */
    HORROR_FROM_THE_DEEP_BASEMENT_UP(new Location(2519, 4618, 1), new Location(2510, 3644, 0)),
    /**
     * The Horror from the deep basement after quest up.
     */
    HORROR_FROM_THE_DEEP_BASEMENT_AFTER_QUEST_UP(new Location(2519, 9994, 1), new Location(2510, 3644, 0)),
    /**
     * Intro leave special ladders.
     */
    INTRO_LEAVE(Location.create(2522, 4999, 0), Location.create(3230, 3240, 0)),
    /**
     * Jatizso mine down special ladders.
     */
    JATIZSO_MINE_DOWN(Location.create(2397, 3812, 0), Location.create(2405, 10188, 0)),
    /**
     * Jatizso mine up special ladders.
     */
    JATIZSO_MINE_UP(Location.create(2406, 10188, 0), Location.create(2397, 3811, 0)),
    /**
     * Jatizso shout tower down special ladders.
     */
    JATIZSO_SHOUT_TOWER_DOWN(Location.create(2373, 3800, 0), Location.create(2374, 3800, 2)),
    /**
     * Jatizso shout tower up special ladders.
     */
    JATIZSO_SHOUT_TOWER_UP(Location.create(2373, 3800, 2), Location.create(2374, 3800, 0)),
    /**
     * Keldagrim library up special ladders.
     */
    KELDAGRIM_LIBRARY_UP(Location.create(2865, 10222, 0), Location.create(2865, 10224, 1)),
    /**
     * Movario's base special ladders.
     */
    MOVARIO_LADDER_UP(Location.create(2036, 4379, 0), Location.create(2502, 3255, 0)),
    /**
     * The Ourania ladder climb down.
     */
    OURANIA_LADDER_CLIMB_DOWN(new Location(2452, 3231, 0), new Location(3271, 4862, 0)),
    /**
     * The Observatory stairs climb down.
     */
    OBSERVATORY_STAIRS_CLIMB_DOWN(new Location(2443, 3159, 1), new Location(2444, 3162, 0)),
    /**
     * The Observatory stairs climb up.
     */
    OBSERVATORY_STAIRS_CLIMB_UP(new Location(2444, 3159, 0), new Location(2442, 3159, 1)),
    /**
     * The Ourania ladder climb up.
     */
    OURANIA_LADDER_CLIMB_UP(new Location(3271, 4862, 0), new Location(2452, 3230, 0)),
    /**
     * The Paterdomus temple staircase north up.
     */
    PATERDOMUS_TEMPLE_STAIRCASE_NORTH_UP(new Location(3415, 3490, 0), new Location(3415, 3491, 1)),
    /**
     * The Paterdomus temple staircase north down.
     */
    PATERDOMUS_TEMPLE_STAIRCASE_NORTH_DOWN(new Location(3415, 3491, 1), new Location(3414, 3491, 0)),
    /**
     * The Paterdomus temple staircase south down.
     */
    PATERDOMUS_TEMPLE_STAIRCASE_SOUTH_DOWN(new Location(3415, 3486, 1), new Location(3414, 3486, 0)),
    /**
     * The Phasmatys bar down.
     */
    PHASMATYS_BAR_DOWN(new Location(3681, 3498, 0), new Location(3682, 9961, 0)),
    /**
     * The Phasmatys bar up.
     */
    PHASMATYS_BAR_UP(new Location(3682, 9962, 0), new Location(3681, 3497, 0)),
    /**
     * The Port sarim rat pits down.
     */
    PORT_SARIM_RAT_PITS_DOWN(new Location(3018, 3232, 0), new Location(2962, 9650, 0)) {
        @Override
        public void checkAchievement(Player player) {
            player.getAchievementDiaryManager().finishTask(player, DiaryType.FALADOR, 1, 11);
        }
    },
    /**
     * The Pirates cove ship center stairs down.
     */
    PIRATES_COVE_SHIP_CENTER_STAIRS_DOWN(new Location(2225, 3807, 1), new Location(2225, 3809, 0)),
    /**
     * The Pirates cove ship center stairs up.
     */
    PIRATES_COVE_SHIP_CENTER_STAIRS_UP(new Location(2225, 3808, 0), new Location(2225, 3806, 1)),
    /**
     * The Pirates cove ship north east stairs down.
     */
    PIRATES_COVE_SHIP_NORTH_EAST_STAIRS_DOWN(new Location(2227, 3806, 3), new Location(2227, 3805, 2)),
    /**
     * The Pirates cove ship north east stairs up.
     */
    PIRATES_COVE_SHIP_NORTH_EAST_STAIRS_UP(new Location(2227, 3806, 2), new Location(2227, 3808, 3)),
    /**
     * The Pirates cove ship north west stairs down.
     */
    PIRATES_COVE_SHIP_NORTH_WEST_STAIRS_DOWN(new Location(2221, 3806, 3), new Location(2221, 3805, 2)),
    /**
     * The Pirates cove ship north west stairs up.
     */
    PIRATES_COVE_SHIP_NORTH_WEST_STAIRS_UP(new Location(2221, 3806, 2), new Location(2221, 3808, 3)),
    /**
     * The Pirates cove ship south east stairs down.
     */
    PIRATES_COVE_SHIP_SOUTH_EAST_STAIRS_DOWN(new Location(2227, 3793, 3), new Location(2227, 3795, 2)),
    /**
     * The Pirates cove ship south east stairs up.
     */
    PIRATES_COVE_SHIP_SOUTH_EAST_STAIRS_UP(new Location(2227, 3794, 2), new Location(2227, 3792, 3)),
    /**
     * The Pirates cove ship south left stairs down.
     */
    PIRATES_COVE_SHIP_SOUTH_LEFT_STAIRS_DOWN(new Location(2221, 3792, 2), new Location(2223, 3792, 1)),
    /**
     * The Pirates cove ship south left stairs up.
     */
    PIRATES_COVE_SHIP_SOUTH_LEFT_STAIRS_UP(new Location(2222, 3792, 1), new Location(2220, 3792, 2)),
    /**
     * The Pirates cove ship south right stairs down.
     */
    PIRATES_COVE_SHIP_SOUTH_RIGHT_STAIRS_DOWN(new Location(2226, 3792, 2), new Location(2225, 3792, 1)),
    /**
     * The Pirates cove ship south right stairs up.
     */
    PIRATES_COVE_SHIP_SOUTH_RIGHT_STAIRS_UP(new Location(2226, 3792, 1), new Location(2228, 3792, 2)),
    /**
     * The Pirates cove ship south west stairs down.
     */
    PIRATES_COVE_SHIP_SOUTH_WEST_STAIRS_DOWN(new Location(2221, 3793, 3), new Location(2221, 3795, 2)),
    /**
     * The Pirates cove ship south west stairs up.
     */
    PIRATES_COVE_SHIP_SOUTH_WEST_STAIRS_UP(new Location(2221, 3794, 2), new Location(2221, 3792, 3)),
    /**
     * The Port sarim rat pits up.
     */
    PORT_SARIM_RAT_PITS_UP(new Location(2962, 9651, 0), new Location(3018, 3231, 0)),
    /**
     * The Seers village spinning house rooftop down.
     */
    SEERS_VILLAGE_SPINNING_HOUSE_ROOFTOP_DOWN(new Location(2715, 3472, 3), new Location(2714, 3472, 1)),
    /**
     * The Seers village spinning house rooftop up.
     */
    SEERS_VILLAGE_SPINNING_HOUSE_ROOFTOP_UP(new Location(2715, 3472, 1), new Location(2714, 3472, 3)) {
        @Override
        public void checkAchievement(Player player) {
            player.getAchievementDiaryManager().finishTask(player, DiaryType.SEERS_VILLAGE, 1, 3);
        }
    },
    /**
     * Swensen down special ladders.
     */
    SWENSEN_DOWN(Location.create(2644, 3657, 0), Location.create(2631, 10006, 0)),
    /**
     * Swensen up special ladders.
     */
    SWENSEN_UP(Location.create(2665, 10037, 0), Location.create(2649, 3661, 0)),
    /**
     * The Tree gnome stronghold west bar stairs down.
     */
    TREE_GNOME_STRONGHOLD_WEST_BAR_STAIRS_DOWN(new Location(2418, 3491, 1), new Location(2419, 3491, 0)),
    /**
     * The Tree gnome stronghold west bar stairs up.
     */
    TREE_GNOME_STRONGHOLD_WEST_BAR_STAIRS_UP(new Location(2417, 3490, 0), new Location(2418, 3492, 1)),
    /**
     * The Waterbirth island dungeon sublevel 2 wallasalki 3 ladder down.
     */
    WATERBIRTH_ISLAND_DUNGEON_SUBLEVEL_2_WALLASALKI_3_LADDER_DOWN(new Location(1799, 4387, 2), new Location(1799, 4388, 1)),
    /**
     * The Waterbirth ladder way to lighthouse 1 up.
     */
    WATERBIRTH_LADDER_WAY_TO_LIGHTHOUSE_1_UP(new Location(1932, 4378, 0), new Location(1932, 4380, 2)),
    /**
     * The Waterbirth ladder way to lighthouse 2 up.
     */
    WATERBIRTH_LADDER_WAY_TO_LIGHTHOUSE_2_UP(new Location(1961, 4391, 2), new Location(1961, 4393, 3)),
    /**
     * The Waterbirth ladder way to lighthouse 3 up.
     */
    WATERBIRTH_LADDER_WAY_TO_LIGHTHOUSE_3_UP(new Location(1975, 4408, 3), new Location(2510, 3644, 0)),
    /**
     * The Werewolf agility course ladder up.
     */
    WEREWOLF_AGILITY_COURSE_LADDER_UP(new Location(3549, 9864, 0), new Location(3543, 3463, 0)),
    /**
     * The Witch house to basement down.
     */
    WITCH_HOUSE_TO_BASEMENT_DOWN(new Location(2907, 3476, 0), new Location(2906, 9876, 0)),
    /**
     * The Witch house to basement up.
     */
    WITCH_HOUSE_TO_BASEMENT_UP(new Location(2907, 9876, 0), new Location(2906, 3476, 0)),
    /**
     * The Wizard tower ladder up.
     */
    WIZARD_TOWER_LADDER_UP(new Location(3103, 9576, 0), new Location(3105, 3162, 0));

    private static HashMap<Location, Location> destinationMap = new HashMap<>();
    private static HashMap<Location, SpecialLadders> ladderMap = new HashMap<>();

    static {
        Arrays.stream(SpecialLadders.values()).forEach(entry -> {
            destinationMap.putIfAbsent(entry.ladderLoc, entry.destLoc);
        });
        Arrays.stream(SpecialLadders.values()).forEach(entry -> {
            ladderMap.putIfAbsent(entry.ladderLoc, entry);
        });
    }

    private Location ladderLoc, destLoc;

    SpecialLadders(Location ladderLoc, Location destLoc) {
        this.ladderLoc = ladderLoc;
        this.destLoc = destLoc;
    }

    public static void add(Location from, Location to) {
        destinationMap.put(from, to);
    }
    public static Location getDestination(Location loc) {
        return destinationMap.get(loc);
    }
    public static SpecialLadders getSpecialLadder(Location loc) {
        return ladderMap.get(loc);
    }

}