package core.net

object Constants {
    const val PORT: Int = 43594
    const val REVISION: Int = 530
    const val CLIENT_BUILD: Int = 1
    const val DEFAULT_MS_IP: String = "127.0.0.1"
}
