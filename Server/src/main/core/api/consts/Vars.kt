package core.api.consts

object Vars {

    const val VARP_QUEST_CHOMPY = 293
    const val VARP_QUEST_ELEMENTAL_WORKSHOP = 299
    const val VARBIT_QUEST_EW_I_QUEST_COMPLETE = 2067
    const val VARP_QUEST_GOBLIN_DIPLOMACY_QUEST_PROGRESS = 62

    const val VARP_QUEST_HOLY_GRAIL_PROGRESS = 5
    const val VARP_QUEST_CLOCK_TOWER_PROGRESS = 10
    const val VARP_QUEST_MERLIN_CRYSTAL_PROGRESS = 14
    const val VARP_QUEST_FIGHT_ARENA_PROGRESS = 17
    const val VARP_QUEST_TEMPLE_OF_IKOV_PROGRESS = 26
    const val VARP_QUEST_COOKS_ASSISTANT_PROGRESS = 29
    const val VARP_QUEST_MONKS_FRIEND_PROGRESS = 30
    const val VARP_QUEST_DORICS_QUEST_PROGRESS = 31
    const val VARP_QUEST_ERNEST_THE_CHICKEN_PROGRESS = 32
    const val VARP_QUEST_SHEEP_HERDER_PROGRESS = 60
    const val VARP_QUEST_RUNE_MYSTERIES_PROGRESS = 63
    const val VARP_QUEST_WATERFALL_PROGRESS = 65
    const val VARP_QUEST_WITCHS_POTION_PROGRESS = 67
    const val VARP_QUEST_BIOHAZARD_PROGRESS = 68
    const val VARP_QUEST_PIRATES_TREASURE_PROGRESS = 71
    const val VARP_QUEST_SCORPION_CATCHER_PROGRESS = 76
    const val VARP_QUEST_DRUIDIC_RITUAL_PROGRESS = 80
    const val VARP_QUEST_RESTLESS_GHOST_PROGRESS = 107
    const val VARP_QUEST_TREE_GNOME_VILLAGE_PROGRESS = 111
    const val VARP_QUEST_OBSERVATORY_QUEST_PROGRESS = 112
    const val VARP_QUEST_SHILO_VILLAGE_PROGRESS = 116
    const val VARP_QUEST_KNIGHTS_SWORD_PROGRESS = 122
    const val VARP_QUEST_BLACK_KNIGHTS_FORTRESS_PROGRESS = 130
    const val VARP_QUEST_DIGSITE_PROGRESS = 131
    const val VARP_QUEST_LEGENDS_QUEST_PROGRESS = 139
    const val VARP_QUEST_ROMEO_JULIET_PROGRESS = 144
    const val VARP_QUEST_SHIELD_OF_ARRAV_PROGRESS = 145
    const val VARP_QUEST_LOST_CITY_PROGRESS = 147
    const val VARP_QUEST_FAMILY_CREST_PROGRESS = 148
    const val VARP_QUEST_THE_GRAND_TREE_PROGRESS = 150
    const val VARP_QUEST_SEA_SLUG_PROGRESS = 159
    const val VARP_QUEST_IMP_CATCHER_PROGRESS = 160
    const val VARP_QUEST_UDERGROUND_PASS_PROGRESS = 161
    const val VARP_QUEST_PLAGUE_CITY_PROGRESS = 165
    const val VARP_QUEST_JUNGLE_POTION_PROGRESS = 175
    const val VARP_QUEST_DRAGON_SLAYER_PROGRESS = 176
    const val VARP_QUEST_VAMPIRE_SLAYER_PROGRESS = 178
    const val VARP_QUEST_SHEEP_SHEARER_PROGRESS = 179
    const val VARP_QUEST_GERTRUDES_CAT_PROGRESS = 180
    const val VARP_QUEST_HEROES_QUEST_PROGRESS = 188
    const val VARP_QUEST_MURDER_MYSTERY_PROGRESS = 192
    const val VARP_QUEST_TOURIST_TRAP_PROGRESS = 197
    const val VARP_QUEST_TRIBAL_TOTEM_PROGRESS = 200
    const val VARP_QUEST_WATCHTOWER_PROGRESS = 212
    const val VARP_QUEST_DEMON_SLAYER_PROGRESS = 222
    const val VARP_QUEST_HAZEEL_CULT_PROGRESS = 223
    const val VARP_QUEST_WTICHS_HOUSE_PROGRESS = 226
    const val VARP_QUEST_PRINCE_ALI_RESCUE_PROGRESS = 273
    const val VARP_QUEST_CHOMPY_PROGRESS = 293
    const val VARP_QUEST_ELEMENTAL_WORKSHOP_PROGRESS = 299
    const val VARP_QUEST_PRIEST_IN_PERIL_PROGRESS = 302
    const val VARP_QUEST_NATURE_SPIRIT_PROGRESS = 307
    const val VARP_QUEST_TROLL_SRONGHOLD_PROGRESS = 317
    const val VARP_QUEST_TAI_BWO_WANNAI_TRIO_PROGRESS = 320
    const val VARP_QUEST_REGICIDE_PROGRESS = 328
    const val VARP_QUEST_EADGARS_RUSE_PROGRESS = 335
    const val VARP_QUEST_SHADES_OF_MORTON_PROGRESS = 339
    const val VARP_QUEST_FREMENNIK_TRIALS_PROGRESS = 347
    const val VARP_QUEST_THRONE_OF_MISCELLANIA_PROGRESS = 359
    const val VARP_QUEST_MONKEY_MADNESS_PROGRESS = 365
    const val VARP_QUEST_HAUNTED_MINE_PROGRESS = 382
    const val VARP_QUEST_TROLL_ROMANCE_PROGRESS = 385
    const val VARP_QUEST_IN_SEARCH_OF_MYREQUE_PROGRESS = 387
    const val VARP_QUEST_CREATURE_OF_FENKENSTRAIN_PROGRESS = 399
    const val VARP_QUEST_ROVING_ELVES_PROGRESS = 402
    const val VARP_QUEST_ONE_SMALL_FAVOR_PROGRESS = 416
    const val VARP_QUEST_LOST_TRIBE_PROGRESS = 465
    const val VARP_QUEST_MOURNINGS_ENDS_PART_I_PROGRESS = 517
    const val VARP_QUEST_RUM_DEAL_PROGRESS = 600
    const val VARP_QUEST_CABIN_FEVER_PROGRESS = 655
    const val VARP_QUEST_RAG_AND_BONE_MAN_PROGRESS = 714
    const val VARP_QUEST_WOLF_WHISTLE_PROGRESS = 1178

    const val VARBIT_QUEST_HORROR_FROM_THE_DEEP_PROGRESS = 34
    const val VARBIT_QUEST_GHOST_AHOY_PROGRESS = 217
    const val VARBIT_QUEST_MOUNTAIN_DAUGHTER_PROGRESS = 260
    const val VARBIT_QUEST_BETWEEN_A_ROCK_PROGRESS = 299
    const val VARBIT_QUEST_THE_FEUD_PROGRESS = 334
    const val VARBIT_QUEST_THE_GOLEM_PROGRESS = 346
    const val VARBIT_QUEST_DESERT_TREASURE_PROGRESS = 358
    const val VARBIT_QUEST_ICTHLARINS_LITTLE_HELPER_PROGRESS = 418
    const val VARBIT_QUEST_TEARS_OF_GUTHIX_PROGRESS = 451
    const val VARBIT_QUEST_GIANT_DWARF_PROGRESS = 571
    const val VARBIT_QUEST_RECRUITMENT_DRIVE_PROGRESS = 657
    const val VARBIT_QUEST_FORGETTABLE_TALE_OF_A_DRUNKEN_DWARF_PROGRESS = 822
    const val VARBIT_QUEST_GARDEN_OF_TRANQUILLITY_PROGRESS = 961
    const val VARBIT_QUEST_THE_GREAT_BRAIN_ROBBERY_PROGRESS = 980
    const val VARBIT_QUEST_A_TAIL_OF_TWO_CATS_PROGRESS = 1028
    const val VARBIT_QUEST_WANTED_PROGRESS = 1051
    const val VARBIT_QUEST_MOURNINGS_END_PART_II_PROGRESS = 1103
    const val VARBIT_QUEST_WHAT_LIES_BELOW_PROGRESS = 3523
    const val VARBIT_QUEST_ALL_FIREDUP_PROGRESS = 1283
    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_PROGRESS = 1372
    const val VARBIT_QUEST_MAKING_HISTORY_PROGRESS = 1383
    const val VARBIT_QUEST_DEVIOUS_MINDS_PROGRESS = 1465
    const val VARBIT_QUEST_RATCATCHERS_PROGRESS = 1404
    const val VARBIT_QUEST_SPIRITS_OF_THE_ELID_PROGRESS = 1444
    const val VARBIT_QUEST_THE_HAND_IN_THE_SAND_PROGRESS = 1527
    const val VARBIT_QUEST_ENAKHRAS_LAMENT_PROGRESS = 1560
    const val VARBIT_QUEST_FAIRY_TALE_I_GROWING_PAINS_PROGRESS = 1803
    const val VARBIT_QUEST_IN_AID_OF_THE_MYREQUE_PROGRESS = 1990
    const val VARBIT_QUEST_A_SOULS_BANE_PROGRESS = 2011
    const val VARBIT_QUEST_SWAN_SONG_PROGRESS = 2098
    const val VARBIT_QUEST_ROYAL_TROUBLE_PROGRESS = 2140
    const val VARBIT_QUEST_DEATH_TO_DORGESHUUN_PROGRESS = 2258
    const val VARBIT_QUEST_FAIRY_TALE_II_CURE_A_QUEEN_PROGRESS = 2326
    const val VARBIT_QUEST_GOBLIN_DIMPLOMACY_QUEST_PROGRESS = 2378
    const val VARBIT_QUEST_LUNAR_DIPLOMACY_PROGRESS = 2448
    const val VARBIT_QUEST_THE_EYE_OF_GLOUPHRIE_PROGRESS = 2497
    const val VARBIT_QUEST_DEMON_SLAYER_PROGRESS = 2561
    const val VARBIT_QUEST_THE_DARKNESS_OF_HALLOWVALE_PROGRESS = 2573
    const val VARBIT_QUEST_THE_SLUG_MENACE_PROGRESS = 2610
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_PROGRESS = 2639
    const val VARBIT_QUEST_EAGLES_PEAK_PROGRESS = 2780
    const val VARBIT_QUEST_GRIM_TALES_PROGRESS = 2783
    const val VARBIT_QUEST_MY_ARMS_BIG_ADVENTURE_PROGRESS = 2790
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_PROGRESS = 2866
    const val VARBIT_QUEST_CONTACT_PROGRESS = 3274
    const val VARBIT_QUEST_COLD_WAR_PROGRESS = 3293
    const val VARBIT_QUEST_FREMENNIK_ISLES_PROGRESS = 3311
    const val VARBIT_QUEST_TOWER_OF_LIFE_PROGRESS = 3337
    const val VARBIT_QUEST_OLAFS_QUEST_PROGRESS = 3534
    const val VARBIT_QUEST_ANOTHER_SLICE_OF_HAM_PROGRESS = 3550
    const val VARBIT_QUEST_DREAM_MENTOR_PROGRESS = 3618
    const val VARBIT_QUEST_KING_RANSOM_PROGRESS = 3888
    const val VARBIT_QUEST_THE_PATH_OF_GLOUPHRIE_PROGRESS = 3954
    const val VARBIT_QUEST_BACK_TO_MY_ROOTS_PROGRESS = 4055
    const val VARBIT_QUEST_LAND_OF_THE_GOBLIN_PROGRESS = 4105
    const val VARBIT_QUEST_DEALING_WITH_SCARABAS_PROGRESS = 4230
    const val VARBIT_QUEST_AS_A_FIRST_RESORT_PROGRESS = 4321
    const val VARBIT_QUEST_CATAPULT_CONSTRUCTION_PROGRESS = 4396
    const val VARBIT_QUEST_KENNITHS_CONCERNS_PROGRESS = 4505
    const val VARBIT_QUEST_LEGACY_OF_SEERGAZE_PROGRESS = 4569
    const val VARBIT_QUEST_PERILS_OF_ICE_MOUNTAIN_PROGRESS = 4684
    const val VARBIT_QUEST_TOKTZ_KET_DRILL_PROGRESS = 4700
    const val VARBIT_QUEST_SMOKING_KILLS_PROGRESS = 4764
    const val VARBIT_QUEST_ROCKING_OUT_PROGRESS = 4797
    const val VARBIT_QUEST_SPIRIT_OF_SUMMER_PROGRESS = 5032
    const val VARBIT_QUEST_MEETING_HISTORY_PROGRESS = 5075
    const val VARBIT_QUEST_SUMMERS_END_PROGRESS = 5331
    const val VARBIT_QUEST_DEFENDER_OF_VARROCK_PROGRESS = 5387
    const val VARBIT_QUEST_SWEPT_AWAY_PROGRESS = 5448
    const val VARBIT_QUEST_WHILE_GUTHIX_SLEEPS_PROGRESS = 5491
    const val VARBIT_QUEST_MYTHS_OF_THE_WHITE_LANDS_PROGRESS = 5733
    const val VARBIT_QUEST_IN_PYRE_NEED_PROGRESS = 5761

    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_READ_BOOK = 2056
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_SLASHED_BOOK = 2057
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_LEFT_WATERCONTROLS = 2058
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_RIGHT_WATERCONTROLS = 2059
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_WATER_WHEEL = 2060
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_LIT_FURNACE = 2062
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_FIXED_BELLOWS = 2063 // FIX 2061 -> PULL 2063
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_ITEM_TRACKER = 2064
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_INITIAL_ENTRY = 2065
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_LEATHER_FOUND = 2066

    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_KEY = 2640
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_HATCH = 2641
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_PIPE = 2650
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_PIN_COG_I = 2655
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_PIN_COG_II = 2656
    const val VARBIT_QUEST_ELEMENTAL_WORKSHOP_II_PIN_COG_III = 2657

    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_ENTRANA_BALLOON = 2867
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_TAVERLEY_BALLOON = 2868
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_CASTLE_WARS_BALLOON = 2869
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_GRAND_TREE_BALLOON = 2870
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_CRAFTING_GUILD_BALLOON = 2871
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_VARROCK_BALLOON = 2872
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_YELLOW_DYE_DELIVERED = 2874
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_SANDBAGS_DELIVERED = 2875
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_SILK_DELIVERED = 2876
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_BOWL_DELIVERED = 2878
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_RED_DYE_DELIVERED = 2879
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_SAND_BAGS_REMAINING = 2880
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_LOGS_REMAINING = 2881
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_BALLOON_DISTANCE = 2882
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_BALLOON_HEIGHT = 2883
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_CURRENT_MAP = 2884
    const val VARBIT_QUEST_BALLOON_TRANSPORT_METHOD_UNLOCKED = 2886
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_SAPLING_LOCK = 2887
    const val VARBIT_QUEST_ENLIGHTENED_JOURNEY_PLANTED_SAPLING_LOCATION = 2888

    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_INCANTATION_ORDER_FIRST = 1373
    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_INCANTATION_ORDER_SECOND = 1374
    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_INCANTATION_ORDER_THIRD = 1375
    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_INCANTATION_ORDER_FOURTH = 1376
    const val VARBIT_QUEST_SHADOW_OF_THE_STORM_INCANTATION_ORDER_FIFTH = 1377

    const val VARBIT_QUEST_DEMON_SLAYER_INCANTATION_ORDER_FIRST = 2562
    const val VARBIT_QUEST_DEMON_SLAYER_INCANTATION_ORDER_SECOND = 2563
    const val VARBIT_QUEST_DEMON_SLAYER_INCANTATION_ORDER_THIRD = 2564
    const val VARBIT_QUEST_DEMON_SLAYER_INCANTATION_ORDER_FOURTH = 2565
    const val VARBIT_QUEST_DEMON_SLAYER_INCANTATION_ORDER_FIFTH = 2566
    const val VARBIT_QUEST_DEMON_SLAYER_SILVERLIGHT_TAKEN = 2567
    const val VARBIT_QUEST_DEMON_SLAYER_DRAIN = 2568
    const val VARBIT_QUEST_DEMON_SLAYER_STONE_BROKEN = 2569

    const val VARBIT_QUEST_OBSERVATORY_QUEST_REWARD_EXPERIENCE_BLOCKER = 3827
    const val VARBIT_QUEST_OBSERVATORY_QUEST_CONSTELLATION = 3828
    const val VARBIT_QUEST_OBSERVATORY_QUEST_VIEWED_CONSTELLATION = 3836
    const val VARBIT_QUEST_OBSERVATORY_GOBLIN_STOVE_LENS_MOULD_TAKEN = 3837
    const val VARBIT_QUEST_OBSERVATORY_TELESCOPE_REPAIRED = 3838
    const val VARBIT_QUEST_OBSERVATORY_DUNGEON_WARNING_COUNTER = 3859

    const val VARBIT_QUEST_TREE_GNOME_VILLAGE_GNOME_FIRST_COORDS = 599
    const val VARBIT_QUEST_TREE_GNOME_VILLAGE_GNOME_SECOND_COORDS = 600
    const val VARBIT_QUEST_TREE_GNOME_VILLAGE_GNOME_THIRD_COORDS = 601

    const val VARBIT_QUEST_ZOGRE_GATE_PASSAGE = 496
    const val VARBIT_QUEST_SITHIK_OGRE_TRANSFORMATION = 495
    const val VARBIT_QUEST_ZORGE_FLESH_EATERS_PROGRESS = 487
    const val VARBIT_QUEST_ZOGRE_COFFIN_TRANSFORM = 488

    const val VARBIT_QUEST_TOURIST_TRAP_CELL_WINDOW = 2801
    const val VARBIT_QUEST_LOST_TRIBE_CAVE_HOLE_STATUS = 532

    const val VARBIT_QUEST_CHOMPY_SPITROAST = 1770

    const val VARBIT_QUEST_THE_FEUD_CAMEL_PROMISES = 315
    const val VARBIT_QUEST_THE_FEUD_RECEIPTS_DELIVERED = 316
    const val VARBIT_QUEST_THE_FEUD_DRUNKEN_ALI_BEER_COUNT = 318
    const val VARBIT_QUEST_THE_FEUD_PAID_STREET_URCHIN = 319
    const val VARBIT_QUEST_THE_FEUD_UNLOCKED_MAYORS_HOUSE = 320
    const val VARBIT_QUEST_THE_FEUD_TRAITOROUS_ALI_INFO = 321
    const val VARBIT_QUEST_THE_FEUD_MENAPHITES_BETRAYED = 322
    const val VARBIT_QUEST_THE_FEUD_BANDITS_BETRAYED = 323
    const val VARBIT_QUEST_THE_FEUD_SAFE_NUMBERS_ENTERED = 325
    const val VARBIT_QUEST_THE_FEUD_SAFE_ENTERING_NUMBERS = 326
    const val VARBIT_QUEST_THE_FEUD_SNAKE_FOR_HAG = 328
    const val VARBIT_QUEST_THE_FEUD_REWARD_GIVEN = 331
    const val VARBIT_QUEST_THE_FEUD_MENAPHITE_LEADER_VISIBLE = 335
    const val VARBIT_QUEST_THE_FEUD_MENAPHITE_LEADER_TELEPORTING = 336
    const val VARBIT_QUEST_THE_FEUD_BANDIT_LEADER_VISIBLE = 338
    const val VARBIT_QUEST_THE_FEUD_JEWELS_HANDED_IN = 339
    const val VARBIT_QUEST_THE_FEUD_PICKPOCKETING_METHODS = 340
    const val VARBIT_QUEST_THE_FEUD_BARMAN_POISON_WARNING = 341
    const val VARBIT_QUEST_THE_FEUD_TRAITOR_DISCOVERED = 342

    const val VARP_SCENERY_ABYSS = 491
    const val VARBIT_SCENERY_ABYSS_OBSTACLES = 625

    const val VARP_IFACE_BANK_INSERT_MODE = 304
    const val VARP_IFACE_BANK_NOTE_MODE = 115

    const val VARP_IFACE_BANK_PIN = 562
    const val VARP_IFACE_BANK_PIN_2 = 563

    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_0 = 997
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_1 = 998
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_2 = 999
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_3 = 1000
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_4 = 1001
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_5 = 1002
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_6 = 1003
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_7 = 1004
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_8 = 1005
    const val VARBIT_IFACE_BANKPIN_MAIN_PIN_SLOT_9 = 1006
    const val VARBIT_IFACE_BANKPIN_MAIN_EXIT_BOX = 1009
    const val VARBIT_IFACE_BANKPIN_MAIN_INFO_DIGIT_TEXT = 1010
    const val VARBIT_IFACE_BANK_INSERT_MODE = 7000
    const val VARBIT_IFACE_BANK_NOTE_MODE = 7001

    const val VARBIT_IFACE_GRAVE_DISPLAY = 4191

    const val VARBIT_FLAG_STOLEN_ITEM_FROM_BAKER_STALL = 158
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_SILK_STALL = 159
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_FUR_STALL = 160
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_SILVER_STALL = 161
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_SPICE_STALL = 162
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_GEM_STALL = 163
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_RELLEKA_FISH_STALL = 164
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_RELLEKA_FUR_STALL = 165
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_FISH_STALL = 166
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_VEG_STALL = 167
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_ETCETERIA_FISH_STALL = 168
    const val VARBIT_FLAG_STOLEN_ITEM_FROM_TEA_STALL = 170

    const val VARBIT_SCENERY_BONE_GRINDER_STATUS = 210

    const val VARBIT_SCENERY_FARMING_PATCH_STATUS_1 = 4771
    const val VARBIT_SCENERY_FARMING_PATCH_STATUS_2 = 4772
    const val VARBIT_SCENERY_FARMING_PATCH_STATUS_3 = 4773
    const val VARBIT_SCENERY_FARMING_PATCH_STATUS_4 = 4774
    const val VARBIT_SCENERY_FARMING_PATCH_STATUS_5 = 4775

    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_EARTH_WARRIOR_BANNER = 1452
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_GHOUL_BANNER = 1453
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_GIANT_BANNER = 1454
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_GOBLIN_BANNER = 1455
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_HOBGOBLIN_BANNER = 1456
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_IMP_BANNER = 1457
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_JOGRE_BANNER = 1458
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_LESSER_DEMON_BANNER = 1459
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_SKELETON_BANNER = 1460
    const val VARBIT_SCENERY_CHAMPIONS_CHALLENGE_ZOMBIE_BANNER = 1461
}
